# Linux

A collection of notes relating to Linux.

## General

- [awesome-sysadmin](https://github.com/kahun/awesome-sysadmin)
- [rtop](https://github.com/rapidloop/rtop)
  - Remote top
- [trapcc](https://github.com/jbangert/trapcc)
  - Computing with traps
- [Intro to SSSD](https://sssd.io/docs/introduction.html)
- [neofetch](https://github.com/dylanaraps/neofetch)
- [tmux cheat sheet](https://tmuxcheatsheet.com/)
- [parallel-ssh](https://github.com/ParallelSSH/parallel-ssh)
- [rsync cheatsheet](https://megamorf.gitlab.io/cheat-sheets/cheat-sheet-rsync/)
- [like-dbg](https://github.com/0xricksanchez/like-dbg)
  - Containerized dev environment for Linux kernel debugging
- [explainshell.com](https://explainshell.com/)
  - Explain how/why a shell command works
  - More so I don't forget an awesome reference for explaining to beginners what's going on
- [Coreutils Gotchas](https://www.pixelbeat.org/docs/coreutils-gotchas.html)

## InfoSec

**TODO:** Some of these made it to another doc.

- [Linux Privilege Escalation using LD_Preload](https://www.hackingarticles.in/linux-privilege-escalation-using-ld_preload/)
- PAM fun
  - [Creating a backdoor in PAM in 5 line of code](https://0x90909090.blogspot.com/2016/06/creating-backdoor-in-pam-in-5-line-of.html)
  - [Exfiltrating credentials via PAM backdoors & DNS requests](https://x-c3ll.github.io/posts/PAM-backdoor-DNS/)
  - [https://github.com/zephrax/linux-pam-backdoor](https://github.com/zephrax/linux-pam-backdoor)
  - [pamdb](https://github.com/ociredefz/pambd): PAM module backdoor
- [BlackArch Tools](https://blackarch.org/tools.html) for further inspiration
- [GTFObins](https://gtfobins.github.io/)
- [Linux Rootkit Tutorial](https://xcellerator.github.io/)
- [BPFDoor - An Evasive Linux Backdoor Technical Analysis](https://www.sandflysecurity.com/blog/bpfdoor-an-evasive-linux-backdoor-technical-analysis/)
- [Using Linux Process Environment Variables for Live Forensics](https://www.sandflysecurity.com/blog/using-linux-process-environment-variables-for-live-forensics/)
- [Linux Command Line Forensics and Intrusion Detection Cheat Sheet](https://www.sandflysecurity.com/blog/compromised-linux-cheat-sheet/)

## ZSH

Check out:

- [What should/shouldn't go in .zshenv, .zshrc, .zlogin, .zprofile, .zlogout?](https://unix.stackexchange.com/a/71258)
- [Tips & Tricks](https://strcat.de/zsh/#tipps)
- [Master Your Z Shell with These Outrageously Useful Tips](http://reasoniamhere.com/2014/01/11/outrageously-useful-tips-to-master-your-z-shell/)
- [ZSH Manual - Options](https://zsh.sourceforge.io/Doc/Release/Options.html)
- [Reddit - Post your .zshrc!](https://www.reddit.com/r/linux/comments/9d0mv/post_your_zshrc/)
- [Zsh Tricks to Blow your Mind](https://www.twilio.com/blog/zsh-tricks-to-blow-your-mind)
