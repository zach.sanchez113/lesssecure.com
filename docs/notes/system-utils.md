# System Utils

**TODO:** Clean up this doc, it's not very good in its current form.

## Notes

- Beware the [Filesystem Hierarychy Standard](https://www.pathname.com/fhs/pub/fhs-2.3.pdf)
- [From Bash to Z Shell](http://www.bash2zsh.com/)

## General

- Thunderbird
  - Not sure how much I like this yet
- MailSpring
- [OnlyOffice](https://www.onlyoffice.com/download-desktop.aspx)
- Signal
- Discord
- [Slack](https://slack.com/downloads/linux)
- 1Password
- gimp
- Audacity
- [Obsidian](https://obsidian.md/)
- Nala (Debian only)
- Browser extensions
  - Dark Reader
  - VT4Browsers
  - VisBug
  - Vue.js devtools
  - [SAML Chrome Panel](https://chrome.google.com/webstore/detail/saml-chrome-panel/paijfdbeoenhembfhkhllainmocckace/related?hl=en)
  - [JSON Formatter](https://chrome.google.com/webstore/detail/json-formatter/bcjindcccaagfpapjjmafapmmgkkhgoa/related)
  - [Wappalyzer](https://chrome.google.com/webstore/detail/wappalyzer-technology-pro/gppongmhjkpfnbhagpmjfkannfbllamg)
  - [SingleFile](https://chrome.google.com/webstore/detail/singlefile/mpiodijhokgodhhofbcjdecpffjipkle?hl=en)
    - Clone webpages
  - [uBlacklist](https://github.com/iorate/ublacklist)
    - Block specific sites from search results
  - uBlock Origin + [uBlock-Origin-dev-filter](https://github.com/quenhus/uBlock-Origin-dev-filter)?
  - [ColorZilla](https://chrome.google.com/webstore/detail/colorzilla/bhlhnicpbhignbdhedgjhgdocnmhomnp)?
  - [Outliner CSS](https://chrome.google.com/webstore/detail/outliner-css/epodomlablfiehjgajhlhbdhidlkokaj)?

## Tools

- [podman](https://podman.io/getting-started/installation)
- [podman-compose](https://github.com/containers/podman-compose)
  - This can either be a pip installation, or the script can be pulled straight from GitHub
- kubectl
- kompose
  - Convert docker-compose to Kubernetes services
- wireshark
- [Burp Suite](https://portswigger.net/burp/releases/community/latest)
- VLC
- Zoom
- Vagrant
- [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher)
- [adb](https://developer.android.com/studio/releases/platform-tools) ([download](https://dl.google.com/android/repository/platform-tools-latest-linux.zip))
- [jd-cli](https://github.com/intoolswetrust/jd-cli)
  - Java decompiler
  - Also check out the GUI!
- [static-toolbox](https://github.com/ernw/static-toolbox)
  - Statically-compiled versions of various Linux utilities
- [static-binaries](https://github.com/andrew-d/static-binaries)
  - More statically-compiled versions of various Linux utilities
- [zsh-bin](https://github.com/romkatv/zsh-bin)
  - Statically-compiled, portable ZSH
  - **NOTE:** Extract this in the directory it will live in!
- [vgrep](https://github.com/vrothberg/vgrep)
  - Pager for grep
- [DBeaver](https://github.com/dbeaver/dbeaver)
  - DB GUI
- [semantic-ssh-key](https://github.com/thialfihar/semantic-ssh-key)
  - Add a message to your pubkeys, because why not
- jq
  - JSON pretty-printing/parsing
- hjson
  - Because sometimes you mess up your JSON and `jq` doesn't want to play nice
- yj
  - Convert between YAML/JSON/TOML/HCL (HashiCorp Configuration Language)
- [gum](https://github.com/charmbracelet/gum)
  - Write glamorous shell scripts
- [early-oom](https://github.com/rfjakob/earlyoom)
  - Better OOM daemon for Linux
- [nohang](https://github.com/hakavlad/nohang)
  - Better OOM daemon for Linux
- [jtm](https://github.com/ldn-softdev/jtm)
  - Convert HTML/XML to JSON and back
- [sqlitebrowser](https://github.com/sqlitebrowser/sqlitebrowser)
  - Frontend for SQLite
- [sql-formatter-plus](https://github.com/kufii/sql-formatter-plus)
  - Whitespace formatter for various query languages

### Rust

These aren't in the setup script, but I may try them out someday:

- [pueue](https://github.com/Nukesor/pueue): Task manager for long-running tasks
- [diskonaut](https://github.com/imsnif/diskonaut): Disk space navigator
- [bandwhich](https://github.com/imsnif/bandwhich): Network stats
- [rip](https://github.com/nivekuil/rip): Like `rm`, but with trashcan
  - cargo rm-improved
- [delta](https://github.com/dandavison/delta): Pager w/ syntax highlighting for git/diff/grep
  - cargo git-delta
- [amp](https://github.com/jmacdonald/amp): vim replacement
- [hyperfine](https://github.com/sharkdp/hyperfine): CLI benchmarking
- [crush](https://github.com/liljencrantz/crush)
  - An alternative shell for Linux
- [nushell](https://github.com/nushell/nushell)
  - Yet another alternative shell

Tip for zellij: `Ctrl+Shift` + drag selection for copying to temporarily take back mouse capturing.

## Programming

- shfmt
- [dotnet (.NET + ASP.NET + F#)](https://docs.microsoft.com/en-us/dotnet/core/install/linux-ubuntu#2004-)
  - https://fsharp.org/use/linux/
- [powershell](https://docs.microsoft.com/en-us/powershell/scripting/install/install-ubuntu?view=powershell-7.2)
- [rust-cli GitHub Project](https://github.com/rust-cli)
  - Libraries for Rust CLI dev

## Dev

- fira code font
- vscode
- git
- git-extras
- postman
- virtualbox
- qemu + kvm + libvirt

## TODO

These will go somewhere someday.

- [gif2txt](https://github.com/hit9/gif2txt)
- [docker-mailserver](https://github.com/docker-mailserver/docker-mailserver)
- [netbox](https://github.com/netbox-community/netbox)
  - Network automation/visualization
- [IVRE](https://ivre.rocks/)
  - Network recon frameworks
- [VimGolf](https://www.vimgolf.com/)
  - Bruh
