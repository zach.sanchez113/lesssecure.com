# Homelab

- [Pulumi](https://www.pulumi.com/)
- Active Directory, via:
  - [Vagrant](https://github.com/rgl/windows-domain-controller-vagrant) - local setup, but takes *forever* to finish since three boxes need to be built...
  - [AWS](https://github.com/PhilKeeble/AWS-RedTeam-ADLab) - more extensive, haven't tested yet
- [BadBlood](https://github.com/davidprowe/BadBlood) - populate a realistic AD environment
- [VyOS](https://vyos.io/) for a router OS


## Resources

- [Entire Internet's BGP Routing Table in our lab: Working with BGP Prefix Origin Validation](https://www.linkedin.com/pulse/entire-bgp-routing-table-our-lab-working-prefix-asier-gonzalez-diaz/)

## Manual ADDS Setup

Reference: https://securitytutorials.co.uk/creating-an-active-directory-home-lab/

First, get the first interface index via:

```
Get-NetIPinterface
```

Then, need to set up static IP + rename computer.

**TODO:** This borks DNS since the tutorial assumes extra networking setup prior to the tutorial.

```shell
New-NetIPAddress -InterfaceIndex 6 -IPAddress 192.168.1.1 -PrefixLength 24 -DefaultGateway 192.168.1.1
Set-DnsClientServerAddress -InterfaceIndex 6 -ServerAddresses 192.168.1.2

Rename-Computer -NewName DC01 -Restart

Reboot-Computer
```

Once that's done, install AD:

```
Install-WindowsFeature AD-Domain-Services -IncludeManagementTools
```

Then create a forest + promote VM to DC:

```
Install-ADDSForest -DomainName "Empire.local" -DomainNetbiosName "empire" -InstallDNS:$true -NoReboot
```

Of course enter a password and say 'yes' to the prompt. After that's done, reboot.

**Be patient.** It'll take 5-10 minutes to finish setting up once it boots.

### Optional: DHCP

TODO: Actual instructions.

```
Install-WindowsFeature DHCP -IncludeManagementTools
netsh dhcp add securitygroups
Restart-service dhcpserver
Add-DhcpServerInDC -DnsName Dc01.empire.local -IPAddress 192.168.1.2
Get-DhcpServerInDC
Add-DhcpServerv4Scope -Name "Empire" -StartRange 192.168.1.1 -EndRange 192.168.1.254 -SubnetMask 255.255.255.0 -State Active
Add-DhcpServerv4ExclusionRange -ScopeId 192.168.1.0 -StartRange 192.168.1.1 -EndRange 192.168.1.49
Set-DhcpServerv4OptionValue -OptionId 3 -value 192.168.1.1 -ScopeId 192.168.1.0
Set-DhcpServerv4OptionValue -DnsDomain empire.local -DnsServer 192.168.1.2
```
