# Rust

- [The Rust Programming Language](https://doc.rust-lang.org/book/title-page.html)
  - Official Rust tutorial
- [Common Newbie Mistakes and Bad Practices in Rust: Bad Habits](https://adventures.michaelfbryan.com/posts/rust-best-practices/bad-habits/)
- [Learning Rust With Entirely Too Many Linked Lists](https://rust-unofficial.github.io/too-many-lists/index.html)
