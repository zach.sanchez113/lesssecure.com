# Data Science

**TODO:** This is just a link dump from bookmarks.

- [data science - Learn Anything](https://learn-anything.xyz/data-science)
- [Data-Science-Cheatsheet/Data_Science_Cheatsheet.pdf at main · aaronwangy/Data-Science-Cheatsheet · GitHub](https://github.com/aaronwangy/Data-Science-Cheatsheet/blob/main/Data_Science_Cheatsheet.pdf)
- [How we achieved write speeds of 1.4 million rows per second | Time series data, faster](https://questdb.io/blog/2021/05/10/questdb-release-6-0-tsbs-benchmark/)
- [Matt Tuck Big data Landscape : bigdata](https://www.reddit.com/r/bigdata/comments/ca521f/matt_tuck_big_data_landscape/)
- [Big Data Pipeline on AWS, Microsoft Azure, and Google Cloud : bigdata](https://www.reddit.com/r/bigdata/comments/mkfsfi/big_data_pipeline_on_aws_microsoft_azure_and/)
