# Frontend

- Vue.js + Vite for any frontend dev
  - `yarn create vite`
- [D3.js](https://d3js.org/)
  - [Examples in Vue.js](https://vuejsexamples.com/tag/d3/)
  - [D3 in Depth](https://www.d3indepth.com/)
- [ace](https://github.com/ajaxorg/ace)
  - Browser-based code editor
