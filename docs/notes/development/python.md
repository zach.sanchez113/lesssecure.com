# Python

- poetry
- pyenv
- pyenv-virtualenv
- [python-future](https://python-future.org/index.html)
  - Python 2/3 compatibility
- [pyautogui](https://pyautogui.readthedocs.io/en/latest/)
  - Automate desktop actions
- [nicewin](https://github.com/asweigart/nicewin)
  - Windows API made easy
- dynaconf
- pytest + pytest-sugar
- [attrs](https://www.attrs.org/en/stable/examples.html)
  - I like this better than dataclasses
- [cookiecutter](https://cookiecutter.readthedocs.io/en/stable/)
  - Project templates
  - For an example: [here](https://github.com/woltapp/wolt-python-package-cookiecutter)
- [mkdocstrings](https://mkdocstrings.github.io/)
  - Sphinx alternative
- [pdoc](https://github.com/mitmproxy/pdoc)
  - Sphinx alternative
- [A Curious Course on Coroutines and Concurrency](https://dabeaz.com/coroutines/)


## TODO

- [pylama](https://klen.github.io/pylama/)
  - Code audit tool
- [radon](https://github.com/rubik/radon)
  - Code metrics
- [eradicate](https://github.com/myint/eradicate)
  - Remove commented-out code
- [vulture](https://github.com/jendrikseipp/vulture)
  - Find dead code
- [Asynchronous Object Initialization - Patterns and Antipatterns](https://web.archive.org/web/20210130220433/http://as.ynchrono.us/2014/12/asynchronous-object-initialization.html)
  - This was linked from somewhere and seemed interesting at a glance
- [howtheysre](https://github.com/upgundecha/howtheysre)
  - Not quite "development", but this is the best place to put this repo
- [CRLF vs. LF: Normalizing Line Endings in Git](https://www.aleksandrhovhannisyan.com/blog/crlf-vs-lf-normalizing-line-endings-in-git/)
  - [Why Windows and Linux line endings don't line up (and how to fix it)](https://developers.redhat.com/blog/2021/05/06/why-windows-and-linux-line-endings-dont-line-up-and-how-to-fix-it)
- [hacker-scripts](https://github.com/NARKOZ/hacker-scripts)
- [Why I use attrs instead of pydantic](https://threeofwands.com/why-i-use-attrs-instead-of-pydantic/#:~:text=attrs%20is%20a%20library%20for,somewhere%2C%20or%20send%20it%20somewhere.)
