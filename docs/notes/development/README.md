# General

Collection of random dev notes/resources.

## General

- pre-commit
- [carbon.now.sh](https://carbon.now.sh/)
  - Beautiful code snippets

## Engineering Blogs

- [Twitch](https://blog.twitch.tv/en/tags/engineering/)

## Canon

- [node-ipc malware](https://www.reddit.com/r/linux/comments/tg9zk1/the_authors_of_nodeipc_have_pushed_malware_in_an/)
  - [Snyk's notification](https://security.snyk.io/vuln/SNYK-JS-NODEIPC-2426370)
- [colors and faker pulled from npm](https://snyk.io/blog/open-source-npm-packages-colors-faker/)
- [party-parrot](https://github.com/santiagobasulto/party-parrot)
  - Get your party on
- [What I Wish I Knew When I Started My Career as a Software Developer](https://lifehacker.com/what-i-wish-i-knew-when-i-started-my-career-as-a-softwa-1681002791)
- [Coding for Violent Psychopaths](https://blog.codinghorror.com/coding-for-violent-psychopaths/)
- [Code for the Maintainer](https://wiki.c2.com/?CodeForTheMaintainer)
- [Making Wrong Code Look Wrong](https://www.joelonsoftware.com/2005/05/11/making-wrong-code-look-wrong/)
- [The Noble Art of Maintenance Programming](https://blog.codinghorror.com/the-noble-art-of-maintenance-programming/)
- [ROT26](http://rot26.org/)
- [Ninja code](https://javascript.info/ninja-code)
- [Code Smells](https://blog.codinghorror.com/code-smells/)
- [The Tao of Programming](https://www.mit.edu/~xela/tao.html)
- [The Door Problem](https://lizengland.com/blog/2014/04/the-door-problem/)
- [My Hardest Bug Ever](https://www.gamedeveloper.com/programming/my-hardest-bug-ever)
- [my_first_calculator.py](https://github.com/AceLewis/my_first_calculator.py)
- [You don't want to build your own minifier](https://www.mullie.eu/dont-build-your-own-minifier/)
- [Hyrum's Law](https://www.hyrumslaw.com/)
- [How to Test](https://matklad.github.io/2021/05/31/how-to-test.html)
- [awesome-falsehood](https://github.com/kdeldycke/awesome-falsehood)
- [How to match HTML with regex](https://stackoverflow.com/questions/1732348/regex-match-open-tags-except-xhtml-self-contained-tags)
- [Systemantics](https://en.wikipedia.org/wiki/Systemantics)
- [Fast inverse square root](https://en.wikipedia.org/wiki/Fast_inverse_square_root)
- [APL](https://en.wikipedia.org/wiki/APL_(programming_language))
  - [trying.apl](https://github.com/vendethiel/trying.apl)
  - [Is APL Dead?](https://www.sacrideo.us/is-apl-dead/)
- [Chef esolang](https://esolangs.org/wiki/Chef)
- [malbolge-lisp](https://github.com/kspalaiologos/malbolge-lisp)
- [git-auf-deutsch](https://github.com/danielauener/git-auf-deutsch)
- [WTF is T_PAAMAYIM_NEKUDOTAYIM](https://phil.tech/2013/wtf-is-t-paamayim-nekudotayim/)
  - [Scope resolution operator](https://en.wikipedia.org/wiki/Scope_resolution_operator#PHP)
- [How do I exit vim?](https://stackoverflow.com/questions/11828270/how-do-i-exit-vim)
- [Deploying Kubernetes clusters in increasingly absurd languages](https://leebriggs.co.uk/blog/2022/05/04/deploying-kubernetes-clusters-in-absurd-languages)
- [U+237C RIGHT ANGLE WITH DOWNWARDS ZIGZAG ARROW](https://ionathan.ch/2022/04/09/angzarr.html)
- [The GNU Manifesto](https://www.gnu.org/gnu/manifesto.en.html)
- [How one programmer broke the internet by deleting a tiny piece of code](https://qz.com/646467/how-one-programmer-broke-the-internet-by-deleting-a-tiny-piece-of-code)
- [The Humble Programmer](https://www.cs.utexas.edu/users/EWD/transcriptions/EWD03xx/EWD340.html)
- [on middle endianness](https://www.middleendian.com/middleendian)
- [Kent Beck's Paint Drip People](https://flowchainsensei.wordpress.com/2021/07/28/kent-becks-paint-drip-people/)
- [URLs: It's complicated...](https://www.netmeister.org/blog/urls.html)
- [What's in a hostname?](https://www.netmeister.org/blog/hostnames.html)
- [Your E-Mail Validation Logic is Wrong](https://www.netmeister.org/blog/email.html)

## References

- [Regular-Expressions.info](https://www.regular-expressions.info/tutorial.html)
  - Good explanations for regexes
- [How to Write Go Code](https://go.dev/doc/code)
  - Official dev tutorial for golang
- Java build tools ref/tutorials since I wasn't paying attention to that in class
  - [Ant](https://ant.apache.org/manual/tutorial-HelloWorldWithAnt.html)
  - [Maven](https://www.baeldung.com/maven)
  - [Maven multi-module](https://www.baeldung.com/maven-multi-module)
