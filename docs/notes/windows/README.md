# Scratch Notes

## Basics

- [List of Microsoft Windows components](https://en.wikipedia.org/wiki/List_of_Microsoft_Windows_components)
- [Architecture of Windows NT](https://en.wikipedia.org/wiki/Architecture_of_Windows_NT)
- [The Component Object Model](https://docs.microsoft.com/en-us/windows/win32/com/the-component-object-model)
- [Common Information Model](https://docs.microsoft.com/en-us/windows/win32/wmisdk/common-information-model)
- [About Windows Management Instrumentation (WMI)](https://docs.microsoft.com/en-us/windows/win32/wmisdk/about-wmi)
  - Uses CIM to represent components
  - Remote connections are made via DCOM (or WinRM)
  - Extra: [Introduction to WMI Basics with PowerShell Part 1](https://www.darkoperator.com/blog/2013/1/31/introduction-to-wmi-basics-with-powershell-part-1-what-it-is.html)
- [WMI Architecture](https://docs.microsoft.com/en-us/windows/win32/wmisdk/wmi-architecture)
  - See the diagram
- [Windows Management Infrastructure (MI)](https://docs.microsoft.com/en-us/previous-versions/windows/desktop/wmi_v2/windows-management-infrastructure)
  - Because that isn't confusing... (MI != WMI)
  - NOTE: seems to be new/a revamp of sorts
- [What are the exact roles of a Windows account's SID, and more specifically its RID, for Windows security?](https://www.itprotoday.com/security/q-what-are-exact-roles-windows-accounts-sid-and-more-specifically-its-rid-windows-security)
  - TL;DR The security ID (SID) can be created by DC or local security authority (LSA), and it has many different (standardized) parts. The last one is the relative ID (RID), which is a unique identifier for whoever issued the SID.
  - MS reference: [link](https://docs.microsoft.com/en-us/windows/security/identity-protection/access-control/security-identifiers)
- [Service overview and network port requirements for Windows](https://docs.microsoft.com/en-US/troubleshoot/windows-server/networking/service-overview-and-network-port-requirements)
- [WMI 4 Atatckers](https://github.com/lutzenfried/Methodology/blob/main/08-%20WMI%204%20Attackers.md)

## PowerShell

- [PowerShell 101](https://docs.microsoft.com/en-us/powershell/scripting/learn/ps101/00-introduction?view=powershell-7.2)
- [PowerShell Cheatsheet](https://ramblingcookiemonster.github.io/images/Cheat-Sheets/powershell-basic-cheat-sheet2.pdf)
- [PowerShell Style Guide](https://poshcode.gitbook.io/powershell-practice-and-style/introduction/readme)

## Random finds

- [xCyclopedia](https://strontic.github.io/xcyclopedia/intro)
  - Reference of all executables, DLLs, etc.
- [Windows X86 System Call Table](https://j00ru.vexillium.org/syscalls/nt/32/)
- [Understanding Windows SysCalls - SysCall Dumper](https://guidedhacking.com/threads/understanding-windows-syscalls-syscall-dumper.14470/)
  - Possibly some interesting stuff in [this forum](https://guidedhacking.com/forums/gamephreakers-game-modding-tutorials.450/)
- [What the hell is COM? part 1](https://eyeofaraven.wordpress.com/programming-stuff/what-the-hell-is-com-part-i/)
- [IPC$ share and null session behavior in Windows](https://docs.microsoft.com/en-us/troubleshoot/windows-server/networking/inter-process-communication-share-null-session)
- [[MS-NRPC]: Netlogon Remote Protocol](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-nrpc/ff8f970f-3e37-40f7-bd4b-af7336e4792f)
- [NSudo](https://github.com/M2Team/NSudo)
  - sysadmin tools
- [WMI Providers for script kiddies](https://www.trustedsec.com/blog/wmi-providers-for-script-kiddies/)
  - Learn more abotu WMI
- [WindowsDllsExport](https://github.com/Mr-Un1k0d3r/WindowsDllsExport)
  - All DLLs exported in C:\Windows\System32\
- [Windows Access Tokens and Alternate Credentials](https://www.cobaltstrike.com/blog/windows-access-tokens-and-alternate-credentials/)
  - Cobalt Strike-specific, but useful for better understanding of how auth works
- [The Defender's Guide to the Windows Registry](https://posts.specterops.io/the-defenders-guide-to-the-windows-registry-febe241abc75)

## Useful Tools

- [nirsoft](https://www.nirsoft.net/)
- [sysinternals](https://docs.microsoft.com/en-us/sysinternals/)
- [chocolatey](https://docs.chocolatey.org/en-us/)
- [Process Hacker](https://processhacker.sourceforge.io/)

## Later

- [Enumerating Registry Hives](https://moyix.blogspot.com/2008/02/enumerating-registry-hives.html)
  - Also see: [Challenges in Carving Registry Hives from Memory](https://moyix.blogspot.com/2007/09/challenges-in-carving-registry-hives.html)
- [PDB Stream Decomposition](https://moyix.blogspot.com/2007/08/pdb-stream-decomposition.html)
- [Auditing the System Call Table](https://moyix.blogspot.com/2008/08/auditing-system-call-table.html)
- [Linking Processes to Users](https://moyix.blogspot.com/2008/08/linking-processes-to-users.html)
- [Memory Registry Tools!](https://moyix.blogspot.com/2009/01/memory-registry-tools.html)
  - Also has pointers to non-memory registry resources
  - For a good orientation to the registry in general, see:
    - [Paper](http://old.dfrws.org/2008/proceedings/p26-dolan-gavitt.pdf)
    - [Presentation](http://old.dfrws.org/2008/proceedings/p26-dolan-gavitt_pres.pdf)
    - [MS registry reference (KB256986)](https://docs.microsoft.com/en-us/troubleshoot/windows-server/performance/windows-registry-advanced-users)
- [The Internal Structure of the Windows Registry](http://suzibandit.ltd.uk/MSc/)
- [(Sys)Call Me Maybe: Exploring Malware Syscalls with PANDA](https://moyix.blogspot.com/2015/08/syscall-me-maybe-exploring-malware.html)
- [Demystifying the "SVCHOST.EXE" Process and Its Command Line Options](https://nasbench.medium.com/demystifying-the-svchost-exe-process-and-its-command-line-options-508e9114e747)
- [WMI Internals Part 3](https://posts.specterops.io/wmi-internals-part-3-38e5dad016be)

## Python ideas

- Python: check out winreg module in stdlib - there's info about HKEYs here
- Script to parse SID?

## From secretsdump.py:

- DCSync ([dead link](]https://github.com/gentilkiwi/kekeo/tree/master/dcsync))
- [SysKey and the SAM](https://moyix.blogspot.com.ar/2008/02/syskey-and-sam.html)
  - NTLM is basically just MD4.
  - SysKey, or the boot key, is a key in the system hive used to encrypt SAM hashes.
  - It's composed of four other separate keys, and must be unscrambled.
  - Before messing w/ SAM, need *hashed* boot key to derive RC4 keys used for decrypting SAM.
  - Semi-complicated, but not too bad. Just need to steal system and SAM hives, or even just SAM if bootkey can be obtained in some other way.
  - For a POC, see: [creddump](https://github.com/moyix/creddump)
- [Decrypting LSA secrets](https://moyix.blogspot.com.ar/2008/02/decrypting-lsa-secrets.html)
  - LSA secrets are in security hive, aiming for NL$KM, the key that encrypts cached domain passwords
    - Of course, the boot key comes into play here
  - Can search memory space of lsass.exe to get this key
  - Key follows a "rolling" (?) decryption scheme (not super important except for dev work)
  - To make things a bit easier, we can also just rip from a separate registry key and do some magic with the boot key
  - See lsadump.py in the creddump repo
- [Cached domain creds](https://moyix.blogspot.com.ar/2008/02/cached-domain-credentials.html)
  - Needed so that users aren't locked out in the event that the DC goes down
  - Found in HKLM\Security\Cache - check values NL$1, NL$2, etc.
  - Just builds off the previous articles, more fun with NL$KM
  - See cachedump.py in the creddump repo

---

- https://web.archive.org/web/20130901115208/www.quarkslab.com/en-blog+read+13
- https://lab.mediaservice.net/code/cachedump.rb
- https://insecurety.net/?p=768
- https://web.archive.org/web/20190717124313/http://www.beginningtoseethelight.org/ntsecurity/index.htm
- https://www.exploit-db.com/docs/english/18244-active-domain-offline-hash-dump-&-forensic-analysis.pdf
- https://www.passcape.com/index.php?section=blog&cmd=details&id=15
