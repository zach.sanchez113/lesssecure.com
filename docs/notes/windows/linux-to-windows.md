# Linux to Windows

One-off notes for any quirkiness I come across learning Windows with a pure Linux background.

- Magic bytes don't matter, file extensions do
- Case-insensitive filepaths -> two files cannot have the "same" name
- Everything is an object in Windows (or a device), not a file
  - Definitely something of an oversimplification, but something to keep in mind
- Administrator and SYSTEM are different
  - SYSTEM is meant to be for the OS only, wheres admin is an actual account
  - SYSTEM has perms that admin doesn't, e.g. the SAM registry key (though admin could override in this particular case)
  - Reference point: https://superuser.com/a/504405
- Windows uses data drives like C:, D:, etc. that each act *kinda* like an FS root
- Windows has a hybrid kernel, not monolithic
  - Useful for lower-level stuff: [Architecture of Windows NT](https://en.wikipedia.org/wiki/Architecture_of_Windows_NT)
- Win32 is a VM of sorts "Virtual DOS Machine", and is the process csrss.exe (Client/Server Runtime Subsystem)
  - Things will likely break if this process is killed
  - https://en.wikipedia.org/wiki/Client/Server_Runtime_Subsystem
  - https://en.wikipedia.org/wiki/Windows_API#Win32
- Session Manager Subsystem (smss.exe) is "PID 1" of user mode
- Registry is commonly used for storing config because apparently people got tired of too many INIs
  - Registry keys can be thought of as folders, and values as files
  - HK(EY) denotes keys at root level of the DB
  - Since it's a DB, there's a transaction log and can guarantee atomicity
  - "Branches" of registry are stored are disk files called hives
  - But not all things are stored on disk, e.g. HKLM\HARDWARE
- HKEY_LOCAL_MACHINE ~= /etc/
- HKEY_CURRENT_USER ~= ~/.config/
- SID is almost like a UUID for users, groups, etc. and is used to create access tokens w/ relevant security data
  - **NOTE:** This (partially?) explains what 'F' and 'V' are: https://en.wikipedia.org/wiki/Security_Identifier#Machine_SIDs
- NTFS uses hidden files to store metadata about other files...?
- You can install package managers for Windows, e.g. Chocolatey
- [NuGet](https://docs.microsoft.com/en-us/nuget/what-is-nuget) is used to manage .NET packages
- There isn't a standard FS layout used by apps
  - This is a big problem with legacy apps (yay backwards-compatibility...)

Handy resource: https://xapax.github.io/security/#basics_of_windows/basics_of_windows/

## Scripting

- [CMD command reference](https://ss64.com/nt/)
- [CMD and Unix Command Equivalents](https://www.lemoda.net/windows/windows2unix/windows2unix.html)
- [PowerShell equivalents for common Linux/bash commands](https://mathieubuisson.github.io/powershell-linux-bash/)
- [Native API Functions](https://www.geoffchappell.com/studies/windows/win32/ntdll/api/native.htm)
