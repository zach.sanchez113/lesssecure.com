# Networking

**Disaclaimer:** The notes here don't necessarily apply just to Windows, they're just more relevant to Windows for my purposes.

These notes are also more scratch notes than a proper reference.

## References

- [[MS-WPO]: Windows Protocols Overview](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-wpo/9e13ef20-8e3c-4e99-bc0d-a344961c87f1)
  - Launchpad for basically everything


## Network Basic Input/Output System (NetBIOS)

This is an API at the session layer of the OSI model (level 5, i.e. above transport) that allows computers to communicate over a LAN.

There are three services:

- NetBIOS-NS (name service) for name registration/resolution
  - Applications must register its NetBIOS name using this service
  - There may be one or more service names, which may be distinct from the Internet host name(s)
- NetBIOS-DGM (datagram distribution service) for connectionless communication
- NetBIOS-SSN (session service) for connection-oriented communication

### NetBIOS over TCP/IP (NBT)

NetBIOS got upgraded in the late 80's to allow encapsulation in TCP/UDP packets.

### NetBIOS Message Block Daemon (NMB)

I was looking at the Impacket code and saw that nmb.py was for NetBIOS, which didn't quite add up to me, and lead me to this SO question: [What does nmbd stand for?](https://superuser.com/questions/636430/what-does-nmbd-stand-for)

TL;DR The nmbd server daemon understands and replies to NetBIOS name service requests such as those produced by SMB/CIFS in Windows-based systems.

Manpage: [nmbd.8](https://www.samba.org/samba/docs/current/man-html/nmbd.8.html)

## Server Message Block (SMB)

SMB provides the following:

- Shared access to files and printers
- An authenticated IPC mechanism
  - NTLM or Kerberos is used for auth

It used to operate over NBT, but that's no longer needed because TCP and UDP are a thing now.

Protocol reference: [[MS-SMB]: Server Message Block (SMB) Protocol](https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-smb/f210069c-7086-4dc2-885e-861d837df688)

### Named Pipes

A pipe is a block of shared memory that can be used for IPC and data exchange.

Named pipes allow two unrelated processes to use this mechanism across a network, and the predefined name makes it possible to tell processes what they should connect to.

Obviously there's some potential here from a red team perspective...

ired.team reference/lab: [Windows NamedPipes 101 + Privilege Escalation](https://www.ired.team/offensive-security/privilege-escalation/windows-namedpipes-privilege-escalation)
