# ESE

- [libesedb](https://github.com/libyal/libesedb)
  - Library/toolchain
- [GH issue for Impacket ESE DB getNextRow](https://github.com/SecureAuthCorp/impacket/issues/503)
- [How is ntds.dit organized? | Habr](https://habr-com.translate.goog/ru/post/172865/?_x_tr_sl=ru&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=sc)
  - Best writeup I've seen thus far
  - [Archive](https://web.archive.org/web/20230111045326/https://habr.com/ru/post/172865/) because who knows how reliable this link to an old post in Russian will be (this version is the original Russian - you'll need to plug it into Google Translate manually)
- [How the Active Directory - Data Store Really Works (Inside NTDS.dit) - Part 1](https://dsblog.azurewebsites.net/?p=762)
  - Good low-level explanations, but is more focused on replication for the meatiest portion
  - Also:
    - [Part 2](https://dsblog.azurewebsites.net/?p=781)
    - [Part 3](https://dsblog.azurewebsites.net/?p=803)
    - [Part 4](https://dsblog.azurewebsites.net/?p=942)

## MS Docs

- [How the Data Store Works](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc772829(v=ws.10)?redirectedfrom=MSDNhttps://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc772829(v=ws.10)?redirectedfrom=MSDN)
- [objectClass, RDN, DN, Constructed Attributes, Secret Attributes](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-adts/85397795-f089-4807-89be-24822570bc2c)
- [ESE Deep Dive: Part 1: The Anatomy of an ESE database](https://techcommunity.microsoft.com/t5/ask-the-directory-services-team/ese-deep-dive-part-1-the-anatomy-of-an-ese-database/ba-p/400496)
  - [Archive](https://web.archive.org/web/20230111044715/https://techcommunity.microsoft.com/t5/ask-the-directory-services-team/ese-deep-dive-part-1-the-anatomy-of-an-ese-database/ba-p/400496) if MS decides to delete for some reason


## TODO

**NOTE:** These are personal scratch notes while trying to deep-dive into the ESE engine/JET API.

In `~/git/Extensible-Storage-Engine/dev/ese/src/eseutil/eseutil.cxx`, look at `wmain`. If we search `modeDump`, we can see what `esentutl` does to init a dump.

For something resembling an info/tutorial, see: `~/git/Extensible-Storage-Engine/test/ese/src/samples/SvcInitSample/SvcInitMainGoo.c`

JET error codes: https://learn.microsoft.com/en-us/windows/win32/extensible-storage-engine/extensible-storage-engine-error-codes

For base JET API, see: `dev/ese/src/ese/jetapi.cxx`

Example funcs:

- `JetAttachDatabaseW`
- `JetCreateDatabaseW`
- `JetOpenDatabaseW`
- `JetGetColumnInfoW`

Don't forget https://github.com/microsoft/ManagedEsent

DumpToCsv.cs: https://github.com/Microsoft/ManagedEsent/blob/master/EsentInteropSamples/DbUtil/DumpToCsv.cs#L108

## Extras

Quality isn't quite as good, but still useful

- [New features in Active Directory Domain Services in Windows Server 2012, Part 18: DNTs Exposed](https://dirteam.com/sander/2012/09/17/new-features-in-active-directory-domain-services-in-windows-server-2012-part-18-dnts-exposed/)
- [ACTIVE DIRECTORY BACKDOORS: Myth or Reality BTA: an open source framework to analyse AD](https://airbus-seclab.github.io/bta/BH_Arsenal_US-15-bta.pdf)
  - BTA -> open-source AD security audit framework, could be useful for more detailed extraction from ESENT
  - [BTA repo](https://github.com/airbus-seclab/bta)
  - Also see: [Airbus security lab publications](https://airbus-seclab.github.io/)
- [How is ntds.dit structured](https://sudonull.com/post/129679-How-is-ntdsdit-structured)
  - **TODO:** Dead link

## Querying AD dumps

**NOTE:** I'm assuming the attributes have been translated from the internal ATTx names. I also screwed up the JSON originally (there are trailing commas), so `hjson` is used to normalize before passing off to `jq`.

Find a specific user by username:

```bash
hjson -j < datatable-not-null.json | jq '.[] | select(.sAMAccountName=="NORMAND_VAZQUEZ")'
```

Find a user by DNT (i.e. distinguishedName, internally the "primary key" of sorts for this user):

```bash
hjson -j < datatable-not-null.json | jq '.[] | select(.DNT_Col==5198)'
hjson -j < datatable-not-null.json | jq '.[] | select(.distinguishedName==5198)'
```

Find this user's parent object (go recursively for the entire human-readable DN!):

```bash
PDNT_col=$(hjson -j < datatable-not-null.json | jq '.[] | select(.DNT_col==5198) | .PDNT_col')
hjson -j < datatable-not-null.json | jq '.[] | select(.DNT_Col=='$PDNT_col')'
```

Find this user's "children":

```bash
hjson -j < datatable-not-null.json | jq '.[] | select(.PDNT_col==5198)'
```

See all group memberships for this user (and possibly others):

```bash
cat link_table.5.json | jq '.[] | select(.backlink_DNT=="5198")'
```

**NOTE:** Yes, the quotes do matter for the link table even though DNT is numeric.

See all members of a group:

```bash
cat link_table.5.json | jq '.[] | select(.link_DNT==7272)'
```
