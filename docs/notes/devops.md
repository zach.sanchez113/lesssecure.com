# DevOps/SRE

- [#ExpandTheEdge: Making Twitter Faster](https://blog.twitter.com/engineering/en_us/topics/infrastructure/2019/expand-the-edge)
- [redisgraph-bulk-loader](https://github.com/RedisGraph/redisgraph-bulk-loader)
- [Twitter data storage and processing](https://ankush-chavan.medium.com/twitter-data-storage-and-processing-dd13fd0fdb30)
