# About

Anything that I need/want to write a proper guide for.


::: tip Disclaimer

These tutorials are currently meant to be on the casual side - I'm not necessarily aiming for a rigorous
treatment of whatever topics. You should read more authoritative sources to supplement them.

While the goal is to spread my knowledge, I may make generalizations at times for readability, and there's
always the chance of minor inaccuracies in the details.

:::
