# Password Cracking

## Cheatsheet

::: note

I have `alias hashcat='hashcat -d 2` in order to use the CUDA backend. Not sure why this isn't automatically
detected/used out of the box, but worth considering.

:::

### Common Hash Types

::: tip

`-D 2` tells hashcat to use the GPU, and `-O` tells hashcat to use an optimized kernel (may not do anything
with some hash types, but it doesn't hurt).

:::

NTLM:

```bash
hashcat -a 0 -m 1000 -O -D 2 \
  -r rules/best64.rule \
  hashes/example.ntlm \
  wordlists/Leaked-Databases/rockyou.txt
```

SSHA-1 (salted SHA-1) from LDAP server:

```bash
hashcat -a 0 -m 111 -O -D 2 \
  -r rules/best64.rule \
  hashes/example.ntlm \
  wordlists/Leaked-Databases/rockyou.txt
```

Kerberoast:

```bash
hashcat -a 0 -m 1000 -O -D 2 \
  -r rules/best64.rule \
  hashes/example.kerberoast \
  wordlists/Leaked-Databases/rockyou.txt
```

AS-REP Roast:

```bash
hashcat -a 0 -m 1000 -O -D 2 \
  -r rules/best64.rule \
  hashes/example.asreproast \
  wordlists/Leaked-Databases/rockyou.txt
```

### Useful Masks

Full character set, up to 8 characters in length:

```bash
hashcat -a 3 -m 1000 -O -D 2 \
  --increment \
  hashes/example.ntlm \
  ?a?a?a?a?a?a?a?a
```

Characters humans are more likely to use, up to 8 characters in length:

```bash
hashcat -a 3 -m 1000 -O -D 2 \
  --increment \
  -1 '?l?u?d._!-@* #/$&\,+=)(??'"'"';]' \
  hashes/example.ntlm \
  ?1?1?1?1?1?1?1?1
```

### Common Toggles/Options

Unleash the full power of your GPU (make sure you aren't running this with a GUI!):

```bash
hashcat -w 4
```

Dump plaintext passwords to a specific potfile:

```bash
hashcat --potfile-path=/path/to/cracked.pot
```

The hashes are formatted `username:hash`:

```bash
hashcat --username
```

Display the crack status every N seconds:

```bash
hashcat --status --status-timer="${NUM_SECONDS}"
```
