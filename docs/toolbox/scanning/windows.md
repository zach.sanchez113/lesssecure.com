# Windows

- [linWinPwn](https://github.com/lefayjey/linWinPwn)
  - Good reference point for helpful options for script-based tools running off of Linux
- [CrackMapExec](https://github.com/byt3bl33d3r/CrackMapExec)
  - A little bit of everything (anything that can be ripped?), but geared towards AD
  - Docs: https://mpgn.gitbook.io/crackmapexec/
- [mfasweep](https://github.com/dafthack/MFASweep)
  - Attempts to log in to various Microsoft services using a provided set of credentials + identify if MFA is enabled
- [smbmap](https://github.com/ShawnDEvans/smbmap)
  - Windows/Samba enum from a Linux host
- [Snaffler](https://github.com/SnaffCon/Snaffler)
  - Search for juicy files in SMB shares
- [enum4linux](https://github.com/CiscoCXSecurity/enum4linux)
  - Enumerate data from Windows and Samba hosts
- [enum4linux-ng](https://github.com/cddmp/enum4linux-ng)
  - Rewrite of enum4linux in Python that can output YAML or JSON
- Standard-ish Linux utilities:
  - [smbclient](https://www.digiater.nl/openvms/decus/vlt95b/net95b/wfw-tcpip/smbclient.1)
    - FTP-like Lan Manager client program
  - [rpcclient](https://www.samba.org/samba/docs/current/man-html/rpcclient.1.html)
    - Tool for executing client side MS-RPC functions
  - [nmblookup](https://www.unix.com/man-page/redhat/1/nmblookup/)
    - NetBIOS over TCP/IP client used to lookup NetBIOS names
- [rdpscan](https://github.com/robertdavidgraham/rdpscan)
  - Scan for "BlueKeep" vuln (CVE-2019-0708)
  - Analysis by Zero Day Initiative: [here](https://www.zerodayinitiative.com/blog/2019/5/27/cve-2019-0708-a-comprehensive-analysis-of-a-remote-desktop-services-vulnerability)
  - Do note that exploitation is very finicky and can lead to BSOD if performed incorrectly. Use any exploits with caution.
- [PowerHuntShares](https://github.com/NetSPI/PowerHuntShares)
  - Look for SMB shares with excessive privileges

## Resources

- [SMB Enumeration & Exploitation & Hardening](https://www.exploit-db.com/docs/48760)
  - Helpful paper
- [Searching SMB Share Files | SANS](https://www.sans.org/blog/searching-smb-share-files/)
