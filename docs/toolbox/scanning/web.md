# Web Enumeration

## General

- OWASP ZAP
- Burp Suite
- [Telerik Fiddler](https://www.telerik.com/fiddler/fiddler-everywhere)
- amass
- gobuster / (auto)dirbuster / feroxbuster / rustbuster
- nikto

## Frameworks

- [w3af](https://github.com/andresriancho/w3af)
  - Basically Metasploit for webapps
- [TIDoS-Framework](https://github.com/0xInfection/TIDoS-Framework)
  - The Offensive Manual Web Application Penetration Testing Framework
  - Basically Metasploit for webapps
- **TODO:** [sparty](https://github.com/0xdevalias/sparty)

## Fingerprinting

- zgrab2
  - **WARNING:** Installation instructions are super shoddy
    - Clone to `/usr/local/src`
    - `cd /usr/local/src/zgrab2/cmd/zgrab2`
    - `go build`
      - Maybe need to `go build` the entire thing first?
  - **WARNING:** May yield false negatives
- wappalyzer
  - Identify technology on websites
- findomain
  - Has paid version
- [fingerprintjs](https://github.com/fingerprintjs/fingerprintjs)
  - Browser fingerprinting library

### SSL

- https://github.com/takeshixx/python-ssllabs
- NSE ssl-cipher-enum

## XSS

- [xsser](https://github.com/epsylon/xsser)
  - Automated XSS
- [xsssniper](https://github.com/gbrindisi/xsssniper)
  - Automated XSS
- [xsscrapy](https://github.com/DanMcInerney/xsscrapy)
  - Automated spider (?) for XSS + SQLi

## SQLi

- sqlmap

## LFI

- [lfimap](https://github.com/hansmach1ne/lfimap)
  - LFI discovery/exploitation

## Fuzzing

- [ffuf](https://github.com/ffuf/ffuf)
  - Fuzzer utility
- [FDsploit](https://github.com/chrispetrou/FDsploit)
  - File inclusion + directory traversal
- [fuzz.txt](https://github.com/Bo0oM/fuzz.txt)
  - Shortlist for files/directories to fuzz for
- [fuzzdb](https://github.com/fuzzdb-project/fuzzdb)
  - General reference point
- [radamsa](https://gitlab.com/akihe/radamsa)
  - General-use fuzzer
  - **This seems to be geared more towards fuzzing a binary**

## WAF Bypass

- [3 tricks to bypass Cloudflare WAF in file upload](https://medium.com/numen-cyber-labs/3-tricks-to-bypass-cloudflare-waf-in-file-upload-9abb83530cfc)

## CMS

- [wpscan](https://github.com/wpscanteam/wpscan)
  - Scan WP sites
  - Beware the payment model (free, but requires registration and comes w/ daily scan limit of ~75 as of writing)
- [joomscan](https://github.com/OWASP/joomscan)
- [droopescan](https://github.com/SamJoan/droopescan)

## Java

### Java RMI

- [remote-method-guesser](https://github.com/qtc-de/remote-method-guesser)
  - Identify common RMI vulns
- [rmiscout](https://github.com/BishopFox/rmiscout)
  - Enumerate remote functions and/or exploit if possible
- [BaRMIe](https://github.com/NickstaDB/BaRMIe)
  - Enumerate and attack RMI services

#### Notes

**TODO: Clean up wording.**

What is Java RMI? It's basically an RPC protocol built for the Java ecosystem, typically used by apps to communicate with other apps/components. It isn't necessarily the most modern approach (as opposed to, say, a REST API), but it'll be around for a while (and I'm sure it's better in some usecases).

For JMX endpoints, look for `RemoteObject` or `RMIServer` in nmap's output. If the JMXRMI endpoint is unauthenticated, you can get a shell on the system with the [exploit/multi/misc/java_jmx_server](https://www.infosecmatter.com/metasploit-module-library/?mm=exploit/multi/misc/java_jmx_server). Worth noting: the scanner part of this tool may lie.

What's JMX? It's be used to monitor and manage a running JVM (read: fewer restrictions on code we can run!), and it typically uses an RMI connector to communicate with clients, hence the mention.

#### Resources

- [HackTricks - Pentesting Java RMI](https://book.hacktricks.xyz/network-services-pentesting/1099-pentesting-java-rmi)
- [Attacking RMI based JMX services](https://mogwailabs.de/en/blog/2019/04/attacking-rmi-based-jmx-services/)
- [Java JMX Server Insecure Configuration RCE](https://support.alertlogic.com/hc/en-us/articles/115005320166-Java-JMX-Server-Insecure-Configuration-RCE)
- [Difference between JMX and RMI](https://stackoverflow.com/a/17607637)


## Java Debug Wireless Protocol

Just use Metasploit: [`exploit/multi/misc/java_jdwp_debugger`](https://www.infosecmatter.com/metasploit-module-library/?mm=exploit/multi/misc/java_jdwp_debugger)


## Misc

- ysoserial
  - Exploit object deserialization vulns
- [CeWL](https://github.com/digininja/CeWL)
  - Custom Word List generator
- [D4N155](https://github.com/OWASP/D4N155)
  - wordlist based on website contents
- [Digestive](https://github.com/eric-conrad/digestive)
  - Dictionary cracking tool for HTTP Digest challenge/response hashes
- [d4js](https://github.com/lelinhtinh/de4js)
  - Deobfuscate JS
  - Not *super* useful, but links to some nice [related projects](https://github.com/lelinhtinh/de4js#related-projects)
- [FOCA](https://github.com/ElevenPaths/FOCA)
  - Extract file metadata + hidden info from docs
- [js-beautify](https://github.com/beautify-web/js-beautify)
  - Beautify JS
- [LinkFinder](https://github.com/GerbenJavado/LinkFinder)
  - Find endpoints in JS
  - Python script
- [linx](https://github.com/riza/linx)
  - Find invisible links in JS
- [interactsh](https://github.com/projectdiscovery/interactsh)
  - Detect OOB interactions, e.g. DNS resolution
- [jndi-injection-exploit](https://github.com/welk1n/JNDI-Injection-Exploit)
  - Generate JNDI payloads
- [CVE-2022-43684: ServiceNow Insecure Access Control leading to Administrator Account Takeover](https://github.com/lolminerxmrig/CVE-2022-43684)
- [wsgidav](https://github.com/mar10/wsgidav)
  - WebDAV server based on WSGI
  - Useful for hosting payloads

## Post

- [BeEF](https://github.com/beefproject/beef)
  - Browser hook
- [wwwolf-php-webshell](https://github.com/WhiteWinterWolf/wwwolf-php-webshell)
  - PHP webshell
- [WebShell](https://github.com/xl7dev/WebShell)
  - A collection of webshells + backdoors
- [reGeorg WebShell](https://github.com/xl7dev/WebShell/tree/master/reGeorg-master)
  - For a tunnel
- [Vailyn](https://github.com/VainlyStrain/Vailyn)
  - Path traversal + LFI
- [Weevely](https://github.com/epinna/weevely3)
  - More advanced PHP webshell
- [Simple Data Exfiltration Through XSS](https://www.trustedsec.com/blog/simple-data-exfiltration-through-xss/)
- [Grabify](https://grabify.link/)
  - Create/track URLs

### Shells

- [PayloadsAllTheThings cheatsheet](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md)
- [reverse-shell-generator](https://github.com/0dayCTF/reverse-shell-generator)
