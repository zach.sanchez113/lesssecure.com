# Cloud

## Tools

- [cloudflair](https://github.com/christophetd/CloudFlair)
  - Find origin servers of websites behind CloudFlare by using Internet-wide scan data from Censys
- [cloud_enum](https://github.com/initstring/cloud_enum)
  - Multi-cloud OSINT tool
- [cloud-service-enum](https://github.com/NotSoSecure/cloud-service-enum)
  - Enumerate cloud servivces that a user has access to
- [stratus-red-team](https://github.com/DataDog/stratus-red-team)
  - Adversary simulation in the cloud
- [CloudFox](https://github.com/BishopFox/cloudfox)
  - Find exploitable paths in cloud infrastructure
- [ScoutSuite](https://github.com/nccgroup/ScoutSuite)
  - Multi-cloud auditing tool
- [cloudscraper](https://github.com/venomous/cloudscraper)
  - Bypass CloudFlare's antibot page
- [hackingthe.cloud](https://hackingthe.cloud/)
  - Encyclopedia of attacks/defenses for cloud services

## AWS

- [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)
  - Official AWS SDK for Python
- [s3scanner](https://github.com/sa7mon/S3Scanner)
  - Enumerate S3 buckets
- [aws_consoler](https://github.com/NetSPI/aws_consoler)
  - Convert AWS CLI creds into AWS console access
- [enumerate-iam](https://github.com/andresriancho/enumerate-iam)
  - Enumerate permissions for AWS credentials
- [aws_stealth_enum_perm](https://github.com/Frichetten/aws_stealth_perm_enum)
  - **WARNING: No longer possible!** This is interesting research I learned about at Black Hat 2023.
  - Blog post: [Enumerate AWS API Permissions Without Logging to CloudTrail](https://frichetten.com/blog/aws-api-enum-vuln/)
- [s3-account-search](https://github.com/WeAreCloudar/s3-account-search)
  - Find the account ID that an S3 bucket belongs to
- [lambda-persistency-poc](https://github.com/twistlock/lambda-persistency-poc)
  - Gain persistency on a Python lambda
- [aws-research-scripts](https://github.com/Frichetten/aws-research-scripts)
  - Various research scripts
  - As of writing, currently only `enum-endpoint.py`, which attempts to find endpoints for a service across all regions and environments (!)
- [aws-api-model-converter](https://github.com/Frichetten/aws-api-model-converter)
  - Convert undocumented APIs in the AWS console into use for the CLI
- [enumate_iam_using_bucket_policy](https://github.com/Frichetten/enumate_iam_using_bucket_policy)
  - Enumerate IAM users/roles by abusing S3 bucket policies

## Azure

- [AzUserEnum](https://github.com/NotSoSecure/AzUserEnum/tree/main)
  - Enumerate valid Azure AD user email IDs
- [Azucar](https://github.com/nccgroup/azucar)
  - Audit Azure security config
- [Stormspotter](https://github.com/Azure/Stormspotter)
  - Azure attack surface
- [ROADtools](https://github.com/dirkjanm/ROADtools)
  - Azure AD framework
- [AzureHound](https://github.com/BloodHoundAD/AzureHound)
  - Presumably like BloodHound
- [Azurite](https://github.com/FSecureLABS/Azurite)
  - Azure enum/recon
- [MicroBurst](https://github.com/NetSPI/MicroBurst)
  - Azure enum + exploitation
- [SkyArk](https://github.com/cyberark/SkyArk)
  - Discover high-privilege accounts
- [PowerZure](https://github.com/hausec/PowerZure)
  - PowerView for Azure

## GCP

- [GCPBucketBrute](https://github.com/RhinoSecurityLabs/GCPBucketBrute)
  - Enumerate GCP buckets, determine what access you have + if there's potential for privilege escalation

## Resources

- PayloadsAllTheThings
  - [Azure Pentest](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Cloud%20-%20Azure%20Pentest.md)
  - [AWS Pentest](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Cloud%20-%20AWS%20Pentest.md)
- [Anonymously Enumerating Azure Services](https://www.netspi.com/blog/technical/cloud-penetration-testing/enumerating-azure-services/)
- [How to Identify Vulnerabilities in Public-Facing Azure Services](https://security.packt.com/identify-vulnerabilities-in-azure/)
- [Azure-Red-Team](https://github.com/rootsecdev/Azure-Red-Team)
  - Collection of tools/scripts
- [NotSoSecure - cloudsecwiki](https://cloudsecwiki.com/)
  - Quite a few resources here
- [AWS CloudTrail vulnerability: Undocumented API allows CloudTrail bypass](https://securitylabs.datadoghq.com/articles/iamadmin-cloudtrail-bypass/)
- [Bypassing CloudTrail in AWS Service Catalog, and Other Logging Research](https://securitylabs.datadoghq.com/articles/bypass-cloudtrail-aws-service-catalog-and-other/)
- [Azure AD Connect for Red Teamers](https://blog.xpnsec.com/azuread-connect-for-redteam/)
