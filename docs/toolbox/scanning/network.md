# Networking

## General

- [attacksurfacemapper](https://github.com/superhedgy/AttackSurfaceMapper)
- [yaptest](https://github.com/pentestmonkey/yaptest)
  - This automates **everything**

### Vuln Scanning

- Nettacker

## Port Scan

- [nmap](https://github.com/nmap/nmap)
  - [NSE script index](https://nmap.org/nsedoc/scripts/)
  - Libraries: https://nmap.org/nsedoc/lib/
- [nmapAutomator](https://github.com/21y4d/nmapAutomator)
  - Shell script that handles nmap scanning for you
- [zmap](https://github.com/zmap/zmap)
  - **WARNING:** This doesn't work over VPN as of writing due to lack of support in `libpcap` ([source](https://github.com/zmap/zmap/issues/316#issuecomment-329868647)). See [burritun](https://github.com/kpcyrd/burritun) for a potential workaround.
- [rustscan](https://github.com/RustScan/RustScan)
- masscan
- [udp-proto-scanner](https://github.com/CiscoCXSecurity/udp-proto-scanner)
  - Faster than UDP scan w/ nmap

## DNS

- zdns
- dnsrecon
- hakrevdns
- quagga (may be dead)
- NSE dns-brute
- MSF auxiliary/gather/enum_dns
- [dnsdumpster.com](https://dnsdumpster.com/)
  - All the DNS enum
- crt.sh
- whois
- [Hurricane Electric BGP Toolkit](https://bgp.he.net/)
- networksdb
- ssl transparency report
- robtex
- SecLists
- [massdns](https://github.com/blechschmidt/massdns)

### References

- [(All) DNS Resource Records](https://www.netmeister.org/blog/dns-rrs.html)

## Databases

- [SQLRecon](https://github.com/skahwah/SQLRecon)
  - MSSQL recon/post-exploit toolkit
- [tnscmd10g](https://gitlab.com/kalilinux/packages/tnscmd10g/-/blob/kali/master/tnscmd10g)
  - Perl script for enumerating Oracle DB TNS listener
- [oscanner](https://gitlab.com/kalilinux/packages/oscanner)
  - Oracle DB enumeration
  - No source...
- [odat](https://github.com/quentinhardy/odat)
  - Oracle DB attacking tool

## VoIP

- [SIPTools](https://github.com/rbagrov/SIPTools)
  - Main toolkit
- [viproy](https://github.com/fozavci/viproy-voipkit)
  - Metasploit module
- [sipvicious](https://github.com/EnableSecurity/sipvicious)
  - Mainly useful for `sipcrack`?
- [VoIPHopper](https://sourceforge.net/projects/voiphopper/)
  - VLAN hopping
- [inviteflood](https://github.com/foreni-packages/inviteflood/tree/master/inviteflood)
  - SIP `INVITE` flood
  - **WARNING:** Haven't 100% read into this, but may be meant purely for DoS
- [SeeYouCM Thief](https://github.com/trustedsec/SeeYouCM-Thief)
  - Grab config files from Cisco Unified Call Manager (CUCM)
- [iCULeak.py](https://github.com/llt4l/iCULeak.py)
  - Find/extract creds from phone config files hosted on Cisco Unified Call Manager (CUCM)

### References

- [Practical VoIP Penetration Testing](https://medium.com/vartai-security/practical-voip-penetration-testing-a1791602e1b4)
- [SeeYouCM-Thief: Exploiting Common Misconfigurations in Cisco Phone Systems](https://www.trustedsec.com/blog/seeyoucm-thief-exploiting-common-misconfigurations-in-cisco-phone-systems/)
- [Unauthenticated Dumping of Usernames via Cisco Unified Call Manager (CUCM)](https://www.n00py.io/2022/01/unauthenticated-dumping-of-usernames-via-cisco-unified-call-manager-cucm/)

## Wireless

- [aioblescan](https://github.com/frawau/aioblescan)
  - Scan/decode BLE info
- [aircrack-ng](https://github.com/aircrack-ng/aircrack-ng)
  - WiFi auditing suite
- [BlueZ](http://www.bluez.org/)
  - Bluetooth stack for Linux
- [btsnoop](https://github.com/joekickass/python-btsnoop)
  - Parse BtSnoop pcap files + encapsulted Bluetooth packets
  - Also, what's this? No source is linked. [btsnoop | PyPi](https://pypi.org/project/btsnoop/)

### References

- [How to Snoop on Bluetooth Devices Using Kali Linux](https://null-byte.wonderhowto.com/how-to/bt-recon-snoop-bluetooth-devices-using-kali-linux-0165049/)

## NAC

- [NACKered](https://github.com/p292/NACKered)
- [nac_bypass](https://github.com/scipag/nac_bypass)
- [macchanger](https://github.com/alobbs/macchanger)

### References

- [Bypassing NAC: A Handy How-To Guide](https://www.scip.ch/en/?labs.20190207)

## VPN

- [vortex](https://github.com/klezVirus/vortex)
  - VPN enum/exploitation toolkit

## Other

**TODO:** Better categorization. This is just a placeholder since they're all (mostly) related.

- [cotopaxi](https://github.com/Samsung/cotopaxi)
  - Testing more esoteric protocols
- [routopsy](https://github.com/sensepost/routopsy)
  - Another toolkit for more esoteric protocols
- [Netenum](https://github.com/redcode-labs/Netenum)
  - Passively discover active hosts by monitoring ARP traffic
- [habu](https://github.com/fportantier/habu)
  - Framework for lots of networking shenanigans
  - Just look at the README
- [EtherPuppet](https://github.com/secdev/etherpuppet)
  - TCP tunneling via virtual interfaces...?
  - Need to read more
- [Chiron](https://github.com/aatlasis/Chiron)
  - IPv6 shenanigans
  - Likely better as a library
- [rsh-grind](https://github.com/pentestmonkey/rsh-grind)
  - Brute-force rsh
- Some user enum scripts from pentestmonkey:
  - [smtp-user-enum](https://github.com/pentestmonkey/smtp-user-enum)
  - [ident-user-enum](https://github.com/pentestmonkey/ident-user-enum)
  - [ftp-user-enum](https://github.com/pentestmonkey/ftp-user-enum)
  - [finger-user-enum](https://github.com/pentestmonkey/finger-user-enum)
- **TIP:** use [pftp](https://www.commandlinux.com/man-page/man1/pftp.1.html)
  - No clue what this is, but may be handy later
- [UDP hole punching](https://en.wikipedia.org/wiki/UDP_hole_punching)
  - NAT traversal technique
- [Enumerating Unix RPC Services](https://etutorials.org/Networking/network+security+assessment/Chapter+12.+Assessing+Unix+RPC+Services/12.1+Enumerating+Unix+RPC+Services/)
  - Because I always forget what I'm even looking at.
- [Router Penetration Testing](https://www.hackingarticles.in/router-penetration-testing/)
