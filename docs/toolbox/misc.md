# Miscellaneous

Things I wouldn't actually be interested in deploying or just don't (necessarily?) fit in the offensive categories.

- [ufonet](https://github.com/epsylon/ufonet)
  - Toolkit for untraceable (D)DoS via P2P (kinda)
- [proxycannon-ng](https://github.com/proxycannon/proxycannon-ng)
  - Private botnet using multiple clouds
- [zeek](https://github.com/zeek/zeek)
  - Network analysis/IDS
- [DeepBlueCLI](https://github.com/sans-blue-team/DeepBlueCLI)
  - Threat hunting Windows event logs
- [Rapid7's Project Sonar Forward DNS Dataset](https://github.com/rapid7/sonar/wiki/Forward-DNS)
  - Other datasets from this project may be quite useful
- [Rootkit Hunter](https://sourceforge.net/p/rkhunter/rkh_code/ci/master/tree/files/)
- [Linux: Have I Been Hacked?](https://flaviu.io/linux-have-i-been-hacked/)
- [BoNeSi](https://github.com/Markus-Go/bonesi)
  - DDoS botnet simulator
- [mrtparse](https://github.com/t2mune/mrtparse)
  - IIRC this is for RIS data
- [nostril](https://github.com/casics/nostril)
  - Nonsense string evaluator
- [albanian-virus](https://github.com/arwinneil/albanian-virus)
  - oh no
- [emba](https://github.com/e-m-b-a/emba)
  - Firmware security analyzer
- [RFID Gooseneck](https://github.com/sh0ckSec/RFID-Gooseneck)
- [exiftool](https://sourceforge.net/projects/exiftool/)
