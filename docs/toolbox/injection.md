# Injection / MITM

## Network

- [Responder](https://github.com/lgandx/Responder)
- [sniffglue](https://github.com/kpcyrd/sniffglue)
  - packet sniffer
  - tcpdump alternative?
- [rshijack](https://github.com/kpcyrd/rshijack)
  - tcp connection hijacker
- [mitm6](https://github.com/dirkjanm/mitm6)
  - Reply to DHCPv6 messages -> become the DNS nameserver -> hijack DNS responses
  - Need to read more
- BPF
  - [Writing BPF code in Rust](https://blog.redsift.com/labs/writing-bpf-code-in-rust/)
  - [redbpf](https://github.com/foniod/redbpf)

## Web

- [mitmproxy](https://github.com/mitmproxy/mitmproxy)
  - HTTP proxy
- [mitmproxy2swagger](https://github.com/alufers/mitmproxy2swagger)
  - Convert mitmproxy captures to Swagger
- [goproxy](https://github.com/elazarl/goproxy)
  - HTTP proxy library for Go
- [Extracting Clear-Text Credentials Directly From Chromium's Memory](https://www.cyberark.com/resources/threat-research-blog/extracting-clear-text-credentials-directly-from-chromium-s-memory)
  - Remediation [here](https://www.cyberark.com/resources/threat-research-blog/go-blue-a-protection-plan-for-credentials-in-chromium-based-browsers). It's not impossible, but quite annoying for a couple vectors.

## Implants

**TODO:** I feel like some other pages have implants that should be migrated here...

- [TeamsImplant](https://github.com/Allevon412/TeamsImplant)
