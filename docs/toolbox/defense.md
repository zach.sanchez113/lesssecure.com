# Defense

I've been collecting some of these as well, just need to place those links/notes in their home.

- [Laurel](https://github.com/threathunters-io/laurel)
  - auditd logs in JSON format to make SIEM ingestion/analysis easier
- [BPFDoor Scanner](https://github.com/snapattack/bpfdoor-scanner)
  - Check for presence of BPFDoor implant
- [Linux-CatScale](https://github.com/FSecureLABS/LinuxCatScale)
  - Collect info from Linux hosts
  - See [this article](https://labs.withsecure.com/tools/cat-scale-linux-incident-response-collection/) about the tool
- [maloverview](https://github.com/alexandreborges/malwoverview)
  - First response tool for malware, URLs, IPs, etc.
- [seccomp-tools](https://github.com/david942j/seccomp-tools)
  - Analyze seccomp profiles
- [bpfdoor-scanner](https://github.com/snapattack/bpfdoor-scanner)
  - Scan for compromised hosts
- [bpf-hookdetect](https://github.com/pathtofile/bpf-hookdetect)
  - Detect syscall hooking w/ eBPF
- [cowrie](https://github.com/cowrie/cowrie)
  - SSH + telnet honeypot
- [threatest](https://github.com/DataDog/threatest)
  - End-to-end TTP tests
  - Also see: [Introducing Threatest, a Go framework for end-to-end testing of threat detection rules](https://securitylabs.datadoghq.com/articles/threatest-end-to-end-testing-threat-detection/)
- [linux-elf-binary-signer](https://github.com/NUAA-WatchDog/linux-elf-binary-signer)
- [email-header-analyzer](https://github.com/cyberdefenders/email-header-analyzer)
- [rip_raw](https://github.com/cado-security/rip_raw)
  - Analyze memory of compromised Linux systems
- [Melody](https://github.com/bonjourmalware/melody)
  - Internet sensor built for CTI
- [Digital Forensics Guide](https://github.com/mikeroyal/Digital-Forensics-Guide)
- [Ghidrathon](https://github.com/mandiant/Ghidrathon)
  - Python 3 support for Ghidra
- [HardeningKitty](https://github.com/scipag/HardeningKitty)
  - Checks + hardens Windows config
- [windows_hardening](https://github.com/0x6d69636b/windows_hardening)
  - Windows hardening settings + config
- [mdec](https://github.com/mborgerson/mdec)
  - Cross-compare output of multiple decompilers
- [dissect](https://docs.dissect.tools/en/latest/index.html)
  - Python libraries meant for DFIR and aren't necessarily easy to use/find otherwise
- [PowerHunt](https://github.com/NetSPI/PowerHunt)
  - Threat-hunting PS1 module to (remotely) identify IOCs at scale based on artifacts associated w/ MITRE techniques
- [Techniques In Email Forensic Analysis](https://digitalinvestigator.blogspot.com/2022/11/techniques-in-email-forensic-analysis.html)
- [Protecting Against Privileged Credential Sprawl (AD)](https://www.hub.trimarcsecurity.com/post/implementing-controls-in-active-directory-protecting-against-privileged-credential-sprawl)
- [Flare VM](https://github.com/mandiant/flare-vm)
  - Mandiant's security distro for malware analysis, IR, pentesting, etc.
- [linux-elf-binary-signer](https://github.com/NUAA-WatchDog/linux-elf-binary-signer)
- [Userland Rootkits are Lame](https://grugq.substack.com/p/userland-rootkits-are-lame)
  - Analyzing Linux userland rootkits tactics + defense measures
  - Inspired by [Symbiote rootkit analysis](https://blogs.blackberry.com/en/2022/06/symbiote-a-new-nearly-impossible-to-detect-linux-threat)
- [Live-patching security vulnerabilities inside the Linux kernel with eBPF Linux Security Module | Cloudflare](https://blog.cloudflare.com/live-patch-security-vulnerabilities-with-ebpf-lsm/)

## SDLC

- [Mozilla SOPS](https://github.com/mozilla/sops)
  - Edit encrypted files to prevent credential leakage
  - For Kubernetes: [KSOPS](https://github.com/viaduct-ai/kustomize-sops)
- [Doppler](https://www.doppler.com/)
  - Secrets manager
- [BitWarden](https://bitwarden.com/)
  - Open-source password manager
  - **TODO:** Does this integrate with source control? Might be mistaken

## Docs

- [NIST SP.800-61: Computer Security Incident Handling Guide](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf)
- [NIST SP.800-86: Guide to Integrating Forensic Techniques into Incident Response](https://nvlpubs.nist.gov/nistpubs/legacy/sp/nistspecialpublication800-86.pdf)
