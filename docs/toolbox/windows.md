# Windows

## Privilege Escalation

- [mimikatz](https://github.com/gentilkiwi/mimikatz)
  - [pypykatz](https://github.com/skelsec/pypykatz)
- [Invoke-TheHash](https://github.com/Kevin-Robertson/Invoke-TheHash)
- [WinPEAS](https://github.com/carlospolop/PEASS-ng/blob/master/winPEAS/winPEASexe/README.md#quick-start)
- [PowerSploit](https://github.com/PowerShellMafia/PowerSploit/)
- [PowerView](https://github.com/PowerShellMafia/PowerSploit/tree/master/Recon)
- [PywerView](https://github.com/the-useless-one/pywerview)
  - Because Python is cool
  - Note that it may not have as many features as PowerView
- [PowerShell Empire](https://github.com/EmpireProject/Empire)
  - Also see the fork that's actually being updated: [BC-SECURITY/Empire](https://github.com/BC-SECURITY/Empire)
  - [Searchable Empire Module Library](https://www.infosecmatter.com/empire-module-library/)
- [UACME](https://github.com/hfiref0x/UACME)
  - Defeat UAC
- [Gopher](https://github.com/EncodeGroup/Gopher)
  - Automatically discover low-hanging fruit
- EDR checks
  - Detect AV/EDR/UBA/whatever
  - Powershell: [Invoke-EDRChecker](https://github.com/PwnDexter/Invoke-EDRChecker)
  - C#: [SharpEDRChecker](https://github.com/PwnDexter/SharpEDRChecker)
- [SMBMap](https://github.com/ShawnDEvans/smbmap)
  - Enumerate samba shares
- [Bypass AppLocker](https://github.com/3gstudent/Bypass-Windows-AppLocker)
- [Run PowerShell w/ DLLs only](https://github.com/p3nt4/PowerShdll)
- [WindowsEnum](https://github.com/absolomb/WindowsEnum)
  - PowerShell script
  - **NOTE:** Kinda old, from 2018 - sanity checks wouldn't be a bad idea
- [windows-privesc-check](https://github.com/pentestmonkey/windows-privesc-check)
  - Standalone executable
  - **NOTE:** Kinda old, from 2015 - sanity checks wouldn't be a bad idea
- [PrintNightmare](https://github.com/ly4k/PrintNightmare)
- [dogwalk](https://github.com/ariary/DogWalk-rce-poc)
  - Abuse `diagcab` file in Outlook to get path traversal -> RCE on boot
- [sam-the-admin](https://github.com/WazeHell/sam-the-admin)
  - Impersonate DA from standard user
  - CVE-2021-42278, CVE-2021-42287
- [EventViewerUAC_BOF](https://github.com/Octoberfest7/EventViewerUAC_BOF)
  - **TODO:** Description
- [siofra](https://github.com/Cybereason/siofra)
  - Identify + exploit DDL hijacking vulns
- [nishang](https://github.com/samratashok/nishang)
  - Offensive PowerShell framework
- [Powerless](https://github.com/gladiatx0r/Powerless)
  - Windows privesc script designed with OSCP labs in mind
- Access Token Manipulation
  - [Primary Access Token Manipulation](https://www.ired.team/offensive-security/privilege-escalation/t1134-access-token-manipulation)
  - Ret2pwn
    - [Part 1](https://xret2pwn.github.io/Access-Token-Part0x01/)
    - [Part 2](https://xret2pwn.github.io/Access-Token-Part0x02/)
    - To be continued?
- [JAWS](https://github.com/411Hall/JAWS)
  - Just Another Windows (enum) Script

### Resources/Articles

- [Windows LPE](https://book.hacktricks.xyz/windows/windows-local-privilege-escalation)
- [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md)
- [Mark-of-the-Web from a red team's perspective](https://outflank.nl/blog/2020/03/30/mark-of-the-web-from-a-red-teams-perspective/)

## Post-Exploit

**TODO:** Clean this up

- [DSInternals](https://github.com/MichaelGrafnetter/DSInternals)
  - Useful for C# dev against Windows
- [(Unofficial) TTP checklist](https://github.com/netbiosX/Checklists/blob/master/Windows-Privilege-Escalation.md)
  - Just for sanity checks
- [Phant0m](https://github.com/hlldz/Phant0m)
  - Kill event logs
- [Koadic](https://github.com/fymore/koadic)
  - Rootkit
  - Dead project?
- [SILENTTRINITY](https://github.com/byt3bl33d3r/SILENTTRINITY)
  - C2/post-exploit framework
- [ssh-agent](https://github.com/PowerShell/Win32-OpenSSH/issues/1487)
  - Dump registry hive `Computer\HKEY_CURRENT_USER\Software\OpenSSH\Agent\Keys`
  - **WARNING:** This is an open issue on GitHub - check to see if MS has fixed this or kept it as "by design"
- [WebClient Abuse (WebDAV)](https://www.thehacker.recipes/ad/movement/mitm-and-coerced-authentications/webclient)
  - Can be useful for coercing auth since it elicits HTTP auth instead of SMB (see PetitPotam)
  - **TODO:** Link an actual tool (CrackMapExec, webclientservicescanner)
- [CreateHiddenAccount](https://github.com/wgpsec/CreateHiddenAccount)
  - Create hidden accounts w/ the registry
- [WDigest Clear-Text Passwords](https://stealthbits.com/blog/wdigest-clear-text-passwords-stealing-more-than-a-hash/)
  - TL;DR You can skip the NTLM hash if this isn't configured correctly
  - Check the `UseLogonCredential` setting
- [suborner](https://github.com/r4wd3r/Suborner)
  - Create a hidden Windows account without invoking APIs that'd trigger event logs
  - The article is really neat too: [Suborner: A Windows Bribery for Invisible Persistence](https://r4wsec.com/notes/the_suborner_attack/index.html)
- [WMEye](https://github.com/pwn1sher/WMEye)
  - Lateral movement over WMI and remote MSBuild execution
- [DumpNParse](https://github.com/icyguider/DumpNParse)
  - Combination LSASS dumper/parser
- [win-brute-logon](https://github.com/DarkCoderSc/win-brute-logon)
  - Install guest account, crack any local users
- [SharpUp](https://github.com/GhostPack/SharpUp)
  - C# port of PowerUp-related functionality
- [lsarelayx](https://github.com/CCob/lsarelayx)
  - NTLM relaying tool
- [Disable Powershell Logging](https://gist.github.com/hook-s3c/7363a856c3cdbadeb71085147f042c1a)
- [Seatbelt](https://github.com/GhostPack/Seatbelt)
  - Host-survery "safety checks"
- [PowerShell-via-CSharp](https://github.com/Alh4zr3d/PowerShell-via-CSharp)
  - More for a reference than anything - this isn't hard in principle
- [pinjectra](https://github.com/SafeBreach-Labs/pinjectra)
  - C/C++ library that implements process injection techniques

### Exploits

- [SystemNightmare](https://github.com/GossiTheDog/SystemNightmare)
  - Print spool abuse
- [Follina](https://doublepulsar.com/follina-a-microsoft-office-code-execution-vulnerability-1a47fce5629e)
  - MS Office URI abuse (specifically `ms-msdt` in this instance)
- [Proxy-Attackchain](https://github.com/FDlucifer/Proxy-Attackchain)
  - Full ProxyLogon attack chain
- [CVE-2022-26809](https://nvd.nist.gov/vuln/detail/CVE-2022-26809)
  - Remote RPC RCE, but exploit code doesn't seem readily available
  - Best candidate: [yuanLink](https://github.com/yuanLink/CVE-2022-26809)
  - May be able to pay [here](https://github.com/HellKnightsCrew/CVE-2022-26809)
- [KernelHub](https://github.com/Ascotbe/Kernelhub)
  - All the kernel exploit POCs

#### Resources/Articles

- [Abusing the MS Office protocol scheme](https://blog.syss.com/posts/abusing-ms-office-protos/)
- [SOCKS Proxy Relaying](https://tw1sm.github.io/2021-02-15-socks-relay/)
- [Microsoft Teams and Skype Logging Privacy Issue](https://www.trustwave.com/en-us/resources/blogs/spiderlabs-blog/microsoft-teams-and-skype-logging-privacy-issue/)
- [Stealing Access Tokens From Office Desktop Applications](https://mrd0x.com/stealing-tokens-from-office-applications/)
- [Hijacking DLLs in Windows](https://www.wietzebeukema.nl/blog/hijacking-dlls-in-windows)
- [ABUSING CVE-2022-26923 THROUGH SOCKS5 ON A MYTHIC C2 AGENT](https://macrosec.tech/index.php/2022/06/01/abusing-cve-2022-26923-through-socks5-on-a-mythic-c2-agent/)
- [Finding Passwords in SYSVOL & Exploiting Group Policy Preferences](https://adsecurity.org/?p=2288)
- [AAD Kill Chain](https://aadinternals.com/aadkillchain/)

### PetitPotam

- [PetitPotam](https://github.com/topotam/PetitPotam.git)
  - Coerce Windows hosts to authenticate to other machines
- [ADCSPwn](https://github.com/bats3c/ADCSPwn)
  - Petitpotam + relay to certificate service
- [DFSCoerce](https://github.com/Wh04m1001/DFSCoerce)
  - **TODO:** description
  - There's an update [here](https://twitter.com/filip_dragovic/status/1538154721655103488)
- [ShadowCoerce](https://github.com/ShutdownRepo/ShadowCoerce)
  - PetitPotam via MS-DFSNM
- [SpoolSample (PrinterBug)](https://github.com/leechristensen/SpoolSample)
  - PetitPotam via MS-RPRN
  - Coerce Windows hosts authenticate to other machines via the MS-RPRN RPC interface

### References

#### PetitPotam

- [MS-EFSR abuse (PetitPotam)](https://www.thehacker.recipes/ad/movement/mitm-and-coerced-authentications/ms-efsr)
- [MS-RPRN abuse (PrinterBug)](https://www.thehacker.recipes/ad/movement/mitm-and-coerced-authentications/ms-rprn)
- [MS-FSRVP abuse (ShadowCoerce)](https://www.thehacker.recipes/ad/movement/mitm-and-coerced-authentications/ms-fsrvp)
- [MS-DFSNM abuse (DFSCoerce)](https://www.thehacker.recipes/ad/movement/mitm-and-coerced-authentications/ms-dfsnm)
- Other articles
  - [[FR] PetitPotam Slides](https://fr.slideshare.net/LionelTopotam/petit-potam-slidesrtfmossir)
  - [Using PetitPotam to NTLM Relay to Domain Administrator](https://www.truesec.com/hub/blog/from-stranger-to-da-using-petitpotam-to-ntlm-relay-to-active-directory)
  - [ired.team Lab](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/adcs-+-petitpotam-ntlm-relay-obtaining-krbtgt-hash-with-domain-controller-machine-certificate)
  - [Derbycon - The Unintended Risks of Trusting Active Directory](https://www.slideshare.net/harmj0y/derbycon-the-unintended-risks-of-trusting-active-directory)

#### Protocols

- [Encrypting File System Remote (MS-EFSRPC)](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-efsr/08796ba8-01c8-4872-9221-1000ec2eff31)
- [Print System Remote Protocol (MS-RPRN)](https://msdn.microsoft.com/en-us/library/cc244528.aspx)
- [File Server Remote VSS Protocol (MS-FSRVP)](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-fsrvp/dae107ec-8198-4778-a950-faa7edad125b)
- [Distributed File System (DFS): Namespace Management Protocol (MS-DFSNM)](https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dfsnm/95a506a8-cae6-4c42-b19d-9c1ed1223979)
