# Other

Less-common OSes that require some special care.

## IBM AIX

- [AIX for Penetration Testers](https://thevivi.net/blog/pentesting/2017-03-19-aix-for-penetration-testers/)

### Scratch Notes

A couple of scratch notes:

- This is a Unix OS, so GNU options in any scripts will cause errors.
- Filesystem layout is practically the same, but a few of the more interesting files aren't in the same place.
- There's a special sysadmin tool called [`smitty`](https://www.ibm.com/docs/en/aix/7.2?topic=s-smitty-command) (System Management Interface Tool (SMIT)) that's used for most tasks.
  - Read: automation should take this into consideration.
