# OSX

- [Process injection: breaking all macOS security layers with a single vulnerability](https://sector7.computest.nl/post/2022-08-process-injection-breaking-all-macos-security-layers-with-a-single-vulnerability/)
- [HackTricks - MacOS Red Teaming](https://book.hacktricks.xyz/macos-hardening/macos-security-and-privilege-escalation/macos-red-teaming)
- [HackTricks - MacOS Security & Privilege Escalation](https://book.hacktricks.xyz/macos-hardening/macos-security-and-privilege-escalation)
- [You're ~~Muted~~ Rooted - Exploiting Zoom on macOS](https://speakerdeck.com/patrickwardle/youre-muted-rooted)
- [Gone Apple Pickin': Red Teaming macOS Environments in 2021](https://media.defcon.org/DEF%20CON%2029/DEF%20CON%2029%20presentations/Cedric%20Owens%20-%20Gone%20Apple%20Pickin%20-%20%20Red%20Teaming%20macOS%20Environments%20in%202021.pdf)
- [MacHound](https://github.com/XMCyber/MacHound)
  - Because of course it exists
- [Bifrost](https://github.com/its-a-feature/bifrost)
  - Kerberos testing
- [macOSTools](https://github.com/xorrior/macOSTools)
  - Offensive toolkit for macOS
