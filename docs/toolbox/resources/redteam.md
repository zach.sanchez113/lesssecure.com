# Red Team

## General

- [HackTricks](https://book.hacktricks.xyz/welcome/readme)
  - The canonical reference
- [The Hacker Recipes](https://www.thehacker.recipes/)
  - Another great reference
- [ired.team](https://www.ired.team/)
  - Another great reference
- [Red team toolkit](https://github.com/infosecn1nja/Red-Teaming-Toolkit)
- [Awesome red team](https://github.com/yeyintminthuhtut/Awesome-Red-Teaming)
- [Searchable Metasploit Module Library](https://www.infosecmatter.com/metasploit-module-library/)
- [The Penetration testers Framework](https://github.com/trustedsec/ptf)
- [Blue Team Con: Going Atomic](https://ajpc500.github.io/talks/Blue-Team-Con-Going-Atomic/)
- [Using Cutting-Edge Threat Simulation to Harden the Microsoft Enterprise Cloud](https://azure.microsoft.com/en-us/blog/red-teaming-using-cutting-edge-threat-simulation-to-harden-the-microsoft-enterprise-cloud/)
- [GhostPack](https://github.com/GhostPack)
  - A number of security tools
- [Adversary Emulation Library](https://github.com/center-for-threat-informed-defense/adversary_emulation_library)
- [MITRE ATT&CK Campaigns](https://attack.mitre.org/campaigns/)
- [Frichetten/tools](https://github.com/Frichetten/tools)
  - A variety of red teaming tools, but notable because a fair few seem pretty specialized

### Tradecraft

- [Red Team Journal](https://www.redteamjournal.com/)
- [Red Teaming Laws (Archive)](https://web.archive.org/web/20190331102335/https://www.redteamjournal.com/red-teaming-laws)
- [Red Teaming: A Balanced View](https://www.bluetoad.com/publication/?i=296986&article_id=2451088&view=articleBrowser&ver=html5)
- [A Primer to Red Teaming](https://bishopfox.com/blog/primer-to-red-teaming)
- [pentest-standard.org](http://www.pentest-standard.org/index.php/Main_Page)

### Public Pentest Reports

- [juliocesarfort's GitHub](https://github.com/juliocesarfort/public-pentesting-reports)
- [pentestreports.com](https://pentestreports.com/reports/)

## Windows

- [Windows domain hardening](https://github.com/PaulSec/awesome-windows-domain-hardening)
- [Windows privesc cheatsheets](https://github.com/HarmJ0y/CheatSheets)
- [Priv2Admin](https://github.com/gtworek/Priv2Admin)
  - Token fun
- [Bypass forbidden cmd.exe and Powershell](https://www.blackhillsinfosec.com/powershell-without-powershell-how-to-bypass-application-whitelisting-environment-restrictions-av/)
- [Null sessions and user enum](https://sensepost.com/blog/2018/a-new-look-at-null-sessions-and-user-enumeration/)
  - rpcclient isn't necessarily as reliable as NSE smb-enum-users - it can yield false negatives. Article provides extra RPC functions (NetLogon/MS-NRPC) for the same/similar purposes
- [AD Pentesting Notes](https://github.com/nirajkharel/AD-Pentesting-Notes)
- [AD Security blog](https://adsecurity.org/)
- [Impacket scripts](https://github.com/SecureAuthCorp/impacket/tree/master/examples)
- [AD-Attack-Defense](https://github.com/infosecn1nja/AD-Attack-Defense)
  - Tons of notes on AD attack/defense
- [Active-Directory-Exploitation-Cheat-Sheet](https://github.com/S1ckB0y1337/Active-Directory-Exploitation-Cheat-Sheet)
- [COM Objects P.1: The Hidden Backdoor in Your System](https://blog.maltrak.com/com-objects-p-1-the-hidden-backdoor-in-your-system-947ac4285e85)
- [Group Policy Search](https://gpsearch.azurewebsites.net/)
- [Windows & Active Directory Exploitation Cheat Sheet and Command Reference](https://casvancooten.com/posts/2020/11/windows-active-directory-exploitation-cheat-sheet-and-command-reference/)
- [Low Level Pleasure](https://repnz.github.io/)
  - Blog about Windows internals, reversing, malware

### Pre-Compiled Binaries

- [SharpCollection](https://github.com/Flangvik/SharpCollection)
- [Ghostpack-CompiledBinaries](https://github.com/r3motecontrol/Ghostpack-CompiledBinaries)
- [impacket_static_binaries](https://github.com/ropnop/impacket_static_binaries)
