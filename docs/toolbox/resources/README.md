# General

Any general resources, articles, etc. I come across that I can't decide on a categorization for.

## InfoSec

- [awesome-security](https://github.com/sbilly/awesome-security)
  - Everything
- [awesome-ctf](https://github.com/apsdehal/awesome-ctf#readme)
  - Tools more specific to CTFs, i.e. there are some defense tools as well
  - Skip the first few sections
- [awesome-hacker-search-engines](https://github.com/edoardottt/awesome-hacker-search-engines)
  - Reference for search engines more relevant to cyber
- [Awesome Bug Bounty Tools](https://github.com/vavkamil/awesome-bugbounty-tools)
- [The Bug Hunter's Methodology](https://github.com/jhaddix/tbhm)
- [Rust security tools (+ more)](https://github.com/rust-unofficial/awesome-rust#security-tools)
- [InviZzzible](https://github.com/CheckPointSW/InviZzzible)
  - Assess sandboxes
- [Puppeteer](https://github.com/puppeteer/puppeteer)
  - Node.js library to control Chrom(e/ium)
  - Also see:
    - [Awesome Puppeteer](https://github.com/transitive-bullshit/awesome-puppeteer)
    - [NYFC (dead?)](https://github.com/jbgrunewald/NYFC)
      - Web crawler using Puppeteer
- [flameshot](https://github.com/flameshot-org/flameshot)
  - Handy screenshot software for pentest in particular, e.g. annotations on the fly

## Coding

- [jc](https://github.com/kellyjonbrazil/jc)
  - Convert output of common CLI tools to JSON

## OSCP

- [OSCP tips and tricks | Unicorn Security](https://therealunicornsecurity.github.io/OSCP/)
  - This has some legitimately useful info/tricks beyond "enumeration!!!"

### Less (immediately) useful, but still interesting

- [android-security-awesome](https://github.com/ashishb/android-security-awesome#readme)
- [PSBits](https://github.com/gtworek/PSBits)
