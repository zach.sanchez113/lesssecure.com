# Application Security

- [Awesome AppSec](https://github.com/paragonie/awesome-appsec)
- OWASP
  - [SecurityShepherd](https://owasp.org/www-project-security-shepherd/) - Web/mobile app training
  - [Cheatsheets](https://cheatsheetseries.owasp.org/)
  - [WSTG (OWASP)](https://owasp.org/www-project-web-security-testing-guide/stable/)
  - [Testing for business logic](https://wiki.owasp.org/index.php/Testing_for_business_logic)
  - Docker
    - [Docker Security Repo](https://github.com/OWASP/Docker-Security)
    - [Docker Security Cheatsheet](https://cheatsheetseries.owasp.org/cheatsheets/Docker_Security_Cheat_Sheet.html)
- [Google Hacking DB (Exploit DB)](https://www.exploit-db.com/google-hacking-database)
- [Big List of Naughty Strings](https://github.com/minimaxir/big-list-of-naughty-strings)
  - **WARNING: Some of these are VERY destructive if successful!**
- [Scaling Appsec at Netflex](https://netflixtechblog.medium.com/scaling-appsec-at-netflix-6a13d7ab6043)
- [Edition 6: Top 4 AppSec metrics and why they are so hard to measure](https://boringappsec.substack.com/p/edition-6-top-4-appsec-metrics-and)
- [cure53 pentest reports](https://cure53.de/#publications)
