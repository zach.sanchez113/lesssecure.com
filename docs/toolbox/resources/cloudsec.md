# Cloud Security

- [Azure IP Ranges and Service Tags - Public Cloud](https://www.microsoft.com/en-us/download/details.aspx?id=56519)
- [CloudSec Docs](https://cloudsecdocs.com/#the-rationale)
- [AADInternals](https://aadinternals.com/)
  - GitHub: [Gerenios/AADInternals](https://github.com/Gerenios/AADInternals)
- [AWS Well-Architected Framework](https://aws.amazon.com/architecture/well-architected/)
  - AWS security framework
