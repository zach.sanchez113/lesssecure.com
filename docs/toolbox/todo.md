# Uncategorized

Just clearing out some tabs and doing a preliminary categorization...

## General Resources

A section explaining various technologies would probably be wise.

- [Designing an Authentication System: a Dialogue in Four Scenes](https://web.mit.edu/kerberos/dialogue.html)
  - Explanation of how Kerberos works
- [Tutorial: Introduction to ldap3](https://ldap3.readthedocs.io/en/latest/tutorial_intro.html)
  - Good LDAP intro
- [malwarejake materials for conferences](https://github.com/malwarejake-public/conference-presentations)

## AppSec

- [HTML Smuggling Explained](https://outflank.nl/blog/2018/08/14/html-smuggling-explained/)
- [fuxploider](https://github.com/almandin/fuxploider): File upload vuln scanner + exploitation
- [JDWP Misconfiguration in Container Images and K8s](https://blog.aquasec.com/jdwp-misconfiguration-container-images)
- [Pwning Your Java Messaging With Deserialization Vulnerabilities](https://www.blackhat.com/docs/us-16/materials/us-16-Kaiser-Pwning-Your-Java-Messaging-With-Deserialization-Vulnerabilities.pdf)
- [Java RMI for pentesters part two — reconnaissance & attack against non-JMX registries](https://itnext.io/java-rmi-for-pentesters-part-two-reconnaissance-attack-against-non-jmx-registries-187a6561314d)
- [GadgetProbe](https://github.com/BishopFox/GadgetProbe)
  - Probe endpoints consuming Java serialized objects to identify classes, libraries, and library versions on remote Java classpaths.
- [Java-Deserialization-Cheat-Sheet](https://github.com/GrrrDog/Java-Deserialization-Cheat-Sheet)
- [jdwp-shellifier](https://github.com/IOActive/jdwp-shellifier)
  - Gain RCE with [Java Debug Wire Protocol (JDWP)](https://docs.oracle.com/javase/8/docs/technotes/guides/troubleshoot/introclientissues005.html)
- [Hacking the Java Debug Wire Protocol - or - "How I met your Java debugger"](https://ioactive.com/hacking-java-debug-wire-protocol-or-how/)
- [REcollapse](https://github.com/0xacb/recollapse)
  - Black-box regex fuzzing to bypass validations and discover normalizations in web applications

## Other

- [Dismantling ZLoader: How malicious ads led to disabled security tools and ransomware](https://www.microsoft.com/security/blog/2022/04/13/dismantling-zloader-how-malicious-ads-led-to-disabled-security-tools-and-ransomware/)
- [The DFIR Report: Quantum Ransomware](https://thedfirreport.com/2022/04/25/quantum-ransomware/)
- [nccgroup: LAPSUS$ TTPs](https://research.nccgroup.com/2022/04/28/lapsus-recent-techniques-tactics-and-procedures/)
- [@cocomelonc](https://cocomelonc.github.io/)
  - Malware analysis/dev
- [ADB Toolkit](https://github.com/ASHWIN990/ADB-Toolkit)
- [AD Forest Recovery - Procedures](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/manage/ad-forest-recovery-procedures)
- [010 Editor](http://www.sweetscape.com/)
- [How to recursively print parent processes](https://social.technet.microsoft.com/Forums/windowsserver/en-US/87b5e231-4832-43ca-92ed-0ab70b6e6726/how-to-recursively-print-process-parent-process-grand-parent-process-great-grand-parent-process?forum=winserverpowershell)
- [burritun](https://github.com/kpcyrd/burritun)
  - Wrap tun interface in tap interface
  - Originally for making tools work over OpenVPN that otherwise couldn't
- [emoji-shellcoding](https://github.com/RischardV/emoji-shellcoding)
  - RISC-V only
- [ImHex](https://github.com/WerWolv/ImHex)
  - Pretty hex editor
- [ZXing Decoder Online](https://zxing.org/w/decode.jspx)
  - Barcode decoder
- [ssdeep](https://ssdeep-project.github.io/ssdeep/index.html)
  - Fuzzy hashes


## Defense

- PowerShell Guides
  - [Windows PowerShell Programmer's Guide](https://docs.microsoft.com/en-us/powershell/scripting/developer/prog-guide/windows-powershell-programmer-s-guide?view=powershell-7.2)
  - [Getting Started with PowerShell on Linux](https://adamtheautomator.com/powershell-linux/)
- [Active Directory Lightweight Directory Services Overview](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-r2-and-2012/hh831593(v=ws.11))
- [libesedb](https://github.com/libyal/libesedb)
  - Python bindings for ESE DB
  - Also see: [esedb-kb](https://github.com/libyal/esedb-kb)

## Red Team

- [Injecting Rogue DNS Records using DHCP](https://www.trustedsec.com/blog/injecting-rogue-dns-records-using-dhcp/)
- [DLP Test Data](https://dlptest.com/sample-data/)
- [Various BOF collection](https://github.com/crypt0p3g/bof-collection)
- [Pivoting with Property Hashes | Shodan](https://help.shodan.io/mastery/property-hashes)
  - E.g. telnet has empty banner -> hash is zero -> can exclude uninteresting results with filter `-hash:0`

### Awesome Repos

- [Awesome Penetration Testing](https://github.com/wtsxDev/Penetration-Testing)
- [awesome-hacking](https://github.com/carpedm20/awesome-hacking)
- [Red-Teaming-TTPs](https://github.com/RoseSecurity/Red-Teaming-TTPs)

### Linux

- [Unset RO variable in bash](https://stackoverflow.com/questions/17397069/unset-readonly-variable-in-bash)
- [How to stop sudo PAM messages in auth.log for a specific user?](https://unix.stackexchange.com/questions/224370/how-to-stop-sudo-pam-messages-in-auth-log-for-a-specific-user)
- [OrBit: New Undetected Linux Threat Uses Unique Hijack of Execution Flow](https://www.intezer.com/blog/incident-response/orbit-new-undetected-linux-threat/)

### Mainframes

Because why not.

- [Mainframed](https://github.com/mainframed/Mainframed)
- [mainframed/Enumeration](https://github.com/mainframed/Enumeration)

### Browsers

- [Chrome Browser Exploitation, Part 1: Introduction to V8 and JavaScript Internals](https://jhalon.github.io/chrome-browser-exploitation-1/)
- [chlonium](https://github.com/rxwx/chlonium)
  - Import/export Chrome cookies

### Exploit Dev

- [ropper](https://github.com/sashs/Ropper)
- [syzkaller](https://github.com/google/syzkaller)
  - Kernel fuzzer
- [Hacking the Apple Webcam (again)](https://www.ryanpickren.com/safari-uxss)
- elfloader
  - [gamozolabs](https://github.com/gamozolabs/elfloader)
  - [trustedsec](https://github.com/trustedsec/ELFLoader)

## Windows-specific

- [hoaxshell](https://github.com/t3l3machus/hoaxshell)
  - Unconventional Windows shell currently undetected due to purely HTTP(S) traffic?
  - Need to see what tricks are at play here
- [SMTP Matching Abuse in Azure AD](https://www.semperis.com/blog/smtp-matching-abuse-in-azure-ad/)
- [Practical guide for golden SAML](https://nodauf.dev/p/practical-guide-for-golden-saml/)
- [DLL Side-loading & Hijacking — Using Threat Intelligence to Weaponize R&D](https://www.mandiant.com/resources/blog/abusing-dll-misconfigurations)
- [Reflective DLL injection](https://github.com/stephenfewer/ReflectiveDLLInjection)
- [certerator](https://github.com/stufus/certerator)
  - Create "signed" binaries on the fly
  - Create a CA, sign a binary, then install CA on target
- [Abusing Trust Account$: Accessing Resources on a Trusted Domain from a Trusting Domain](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/abusing-trust-accountusd-accessing-resources-on-a-trusted-domain-from-a-trusting-domain)
- [Attacking and Remediating Excessive Network Share Permissions in Active Directory Environments](https://www.netspi.com/blog/technical/network-penetration-testing/network-share-permissions-powerhuntshares/)
- [Invoke-SocksProxy](https://github.com/p3nt4/Invoke-SocksProxy)
- [RpcView](https://github.com/silverf0x/RpcView)
  - Useful for exploring/decompiling RPC interfaces
- [Offensive Windows IPC Internals 1: Named Pipes](https://csandker.io/2021/01/10/Offensive-Windows-IPC-1-NamedPipes.html)
  - This is a series!
- [CVE-2022-30216 - Authentication coercion of the Windows "Server" service](https://www.akamai.com/blog/security/authentication-coercion-windows-server-service)
- [How to secure a Windows RPC Server, and how not to.](https://www.tiraniddo.dev/2021/08/how-to-secure-windows-rpc-server-and.html)
- [LNK File Analysis: LNKing It Together!](https://syedhasan010.medium.com/forensics-analysis-of-an-lnk-file-da68a98b8415)
  - Also: [SentnelOne - Understanding How .LINK Files Work](https://www.sentinelone.com/blog/windows-shortcut-file-lnk-sneaking-malware/)
- [SentinelOne - Inside Malicious Windows Apps for Malware Deployment](https://www.sentinelone.com/labs/inside-malicious-windows-apps-for-malware-deployment/)
- [SentinelOne - Who Needs Macros? | Threat Actors Pivot to Abusing Explorer and Other LOLBins via Windows Shortcuts](https://www.sentinelone.com/labs/who-needs-macros-threat-actors-pivot-to-abusing-explorer-and-other-lolbins-via-windows-shortcuts/)
- [NTLMquic](https://blog.xpnsec.com/ntlmquic/)
  - Repo: [NTLMquic POC Collection](https://github.com/xpn/ntlmquic)
- [CanaryHunter](https://github.com/C0axx/CanaryHunter)
  - PowerShell script to check for Common Canaries
- [I'M BRINGING RELAYING BACK: A COMPREHENSIVE GUIDE ON RELAYING ANNO 2022](https://www.trustedsec.com/blog/a-comprehensive-guide-on-relaying-anno-2022/)
- [SharpRDP](https://github.com/0xthirteen/SharpRDP)
  - RDP app for the CLI that performs command execution
- [Blackbyte Ransomware Bypass EDR Security Using Drive Vulnerability](https://gbhackers.com/blackbyte-ransomware-bypass-edr-security/)
  - "Bring Your Own Vulnerable Driver" (BYOVD) attack analysis
- [NTFS.com](https://www.ntfs.com/index.html)
  - Technical reference for NTFS
- [Export-MFT.ps1](https://gist.github.com/secabstraction/4044f4aadd3ef21f0ca9)
  - Extracts master file table from volume
- [ebpf-for-windows](https://github.com/microsoft/ebpf-for-windows)
- [TcbElevation.cpp](https://gist.github.com/antonioCoco/19563adef860614b56d010d92e67d178)
  - TODO: What even is this...?
- [TamperingAllArgumentsSyscalls.cpp](https://github.com/rad9800/misc/blob/main/TamperingAllArgumentsSyscalls.cpp)
  - TODO: Dead link, need to find archive
- [pylnk](https://github.com/strayge/pylnk)
  - Python lib for reading/writing Windows shortcut files (.lnk)
- [User-Behavior-Mapping-Tool](https://github.com/trustedsec/User-Behavior-Mapping-Tool)
  - Project aims to map out common user behavior on the computer


## Uncategorized

- [Emotet's Uncommon Approach of Masking IP Addresses](https://www.mcafee.com/blogs/other-blogs/mcafee-labs/emotets-uncommon-approach-of-masking-ip-addresses/)
- [zerosum0x0-archive](https://github.com/zerosum0x0-archive/archive)
  - Loads of handy tools that to check out later
- [SIET](https://github.com/frostbits-security/SIET)
  - Attack Cisco devices by exploiting lack of auth for Cisco Smart Install
- [drltrace](https://github.com/mxmssh/drltrace)
  - Library calls tracer for Windows and Linux applications
- [DanderSpritz](https://github.com/francisck/DanderSpritz_lab)
  - Also see: [DoubleFeature (CheckPoint Research)](https://research.checkpoint.com/2021/a-deep-dive-into-doublefeature-equation-groups-post-exploitation-dashboard/)
- [Shared Sections for Code Injection](https://billdemirkapi.me/sharing-is-caring-abusing-shared-sections-for-code-injection/)
- [gost](https://github.com/ginuerzh/gost): Simple tunnel written in golang
- [KrbRelayUp](https://github.com/Dec0ne/KrbRelayUp): Privesc when LDAP signing isn't enforced
- [How Attackers Dump Active Directory Database Credentials](https://adsecurity.org/?p=2398) (for reference)
- [icmpdoor - ICMP Reverse Shell](https://github.com/krabelize/icmpdoor)
  - Article: [icmpdoor - ICMP reverse shell in Python 3](https://cryptsus.com/blog/icmp-reverse-shell.html)
- [Practical VoIP Penetration Testing](https://medium.com/vartai-security/practical-voip-penetration-testing-a1791602e1b4)
- [Awesome Anti-Forensics](https://github.com/shadawck/awesome-anti-forensic)
- [SharpHound](https://github.com/BloodHoundAD/SharpHound)
  - Of course, [RustHound](https://github.com/OPENCYBER-FR/RustHound)
- [MS Red Teaming Strategy](https://azure.microsoft.com/en-us/blog/red-teaming-using-cutting-edge-threat-simulation-to-harden-the-microsoft-enterprise-cloud/)
- [Docker Security Playground](https://github.com/DockerSecurityPlayground/DSP)
- [Qualys: Pwnkit](https://blog.qualys.com/vulnerabilities-threat-research/2022/01/25/pwnkit-local-privilege-escalation-vulnerability-discovered-in-polkits-pkexec-cve-2021-4034): pkexec exploit
- [mod_rootme](https://github.com/sajith/mod-rootme): Ye olde root shell for ye olde httpd
- [flAWS challenge](http://flaws.cloud/): Cloud security CTF
- [flAWS2 Challenge](http://flaws2.cloud/): Cloud security CTF
- [Removing Kernel Callbacks Using Signed Drivers](https://br-sn.github.io/Removing-Kernel-Callbacks-Using-Signed-Drivers/)
- [PPLKiller](https://github.com/RedCursorSecurityConsulting/PPLKiller): Kernel-mode driver that disables LSA protection (Protected Process Light)
  - NOTE: Different repo than [this PPLKiller](https://github.com/Mattiwatti/PPLKiller)
- [Finding Evil in DNS Traffic](https://www.irongeek.com/i.php?page=videos/bsidesaugusta2016/living-in-america02-finding-evil-in-dns-traffic-keelyn-roberts)
- [Finding Pwned Passwords in Active Directory](https://safepass.me/2020/02/25/finding-pwned-passwords-in-active-directory/)
- [EvilClippy](https://github.com/outflanknl/EvilClippy): Clippy now helps you create malicious docs
- [Adversary Tactics: PowerShell](https://github.com/specterops/at-ps)
- [A Little Guide to SMB Enumeration](https://www.hackingarticles.in/a-little-guide-to-smb-enumeration/)
- [BeRoot](https://github.com/AlessandroZ/BeRoot): Linux privesc
- [pupy](https://github.com/n1nj4sec/pupy/): Cross-platform RAT/post-exploit tool (written mostly in Python!)
- [CheckPoint Research: EternalBlue analysis](https://research.checkpoint.com/2017/eternalblue-everything-know/)
- [Malware Analysis: syscalls](https://jmpesp.me/malware-analysis-syscalls-example/)
- [Process Hollowing](https://github.com/m0n0ph1/Process-Hollowing)
- [DDoS with WS-Discovery Protocol](https://www.techrepublic.com/article/a-new-type-of-ddos-attack-can-amplify-attack-strength-by-more-than-15300/)
- [A Samba's horror story, CVE-2021-44142](https://0xsha.io/blog/a-samba-horror-story-cve-2021-44142)
  - There's also a POC [here](https://github.com/horizon3ai/CVE-2021-44142)
- [GOAD (Game of Active Directory)](https://github.com/Orange-Cyberdefense/GOAD)
  - AD lab
- [Analysis of a Convoluted Attack Chain Involving Ngrok](https://www.trendmicro.com/en_us/research/20/i/analysis-of-a-convoluted-attack-chain-involving-ngrok.html)
- [Fantastic Rootkits: And Where to Find Them (Part 1)](https://www.cyberark.com/resources/threat-research-blog/fantastic-rootkits-and-where-to-find-them-part-1)
- [Egress-Assess](https://github.com/FortyNorthSecurity/Egress-Assess)
  - Test egress data detection capabilities
- [CVE-2019-16098 POC](https://github.com/Barakat/CVE-2019-16098)
  - The driver in Micro-Star MSI Afterburner 4.6.2.15658 (aka RTCore64.sys and RTCore32.sys) allows any authenticated user to read and write to arbitrary memory, I/O ports, and MSRs.
  - **WARNING:** Hardcoded Windows 10 x64 Version 1903 offsets!
- [linux-magazin.de](https://www.linux-magazin.de/)
- [Intro to Quantum Computing](https://csferrie.medium.com/introduction-to-quantum-computing-df9e1182a831)
