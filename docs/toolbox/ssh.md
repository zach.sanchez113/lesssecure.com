# SSH

- [ssh-audit](https://github.com/jtesta/ssh-audit)
  - Grab banner, check for vulns, weak algos, etc.
- [ssh-badkeys](https://github.com/rapid7/ssh-badkeys)
  - Default/known SSH keys
- [kompromat](https://github.com/BenBE/kompromat)
  - Default/known SSH keys
- [Playing with PuTTY](https://labs.withsecure.com/blog/playing-with-putty/)
  - PuTTY abuse
- [SSHD Injection and Password Harvesting](https://jm33.me/sshd-injection-and-password-harvesting.html)
- [systemd service for autossh](https://gist.github.com/ttimasdf/ef739670ac5d627981c5695adf4c8f98)
- [sshuttle](https://github.com/sshuttle/sshuttle)
  - Transparent proxy server that works as a poor man's VPN. Forwards over ssh.

## References/Articles

- [A Visual Guide to SSH Tunnels: Local and Remote Port Forwarding](https://iximiuz.com/en/posts/ssh-tunnels/)
  - This helps me a lot with figuring out how to tunnel something
