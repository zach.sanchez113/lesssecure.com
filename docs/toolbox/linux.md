# Linux

## Privilege Escalation

- [linpeas](https://github.com/carlospolop/PEASS-ng/tree/master/linPEAS)
  - Get it from releases page, or `curl -L https://github.com/carlospolop/PEASS-ng/releases/latest/download/linpeas.sh | sh`
  - **NOTE:** This remvoes the need for LinEnum and linux-exploit-suggester-2
- [LinEnum](https://github.com/rebootuser/LinEnum)
- [linux-exploit-suggester-2](https://github.com/jondonas/linux-exploit-suggester-2)
  - Not to be confused with [the original](https://github.com/InteliSecureLabs/Linux_Exploit_Suggester), which was last updated in 2014
- [linux-smart-enumeration](https://github.com/diego-treitos/linux-smart-enumeration)
- [unix-privesc-check](https://github.com/pentestmonkey/unix-privesc-check)
- [sudo_killer](https://github.com/TH3xACE/SUDO_KILLER)
  - Find fun sudo perms
- [traitor](https://github.com/liamg/traitor)
  - Auto-exploit vulnerabilities to get a root shell
- [suid3num](https://github.com/Anon-Exploiter/SUID3NUM.git)
  - Find interesting suid binaries
- [pspy](https://github.com/DominicBreuker/pspy)
  - Monitor processes w/o root

### Exploits

- [Dirty COW](https://dirtycow.ninja/)
- [CVE-2022-23222](https://github.com/tr3ee/CVE-2022-23222)
  - eBPF privilege escalation in kernel version 5.8 <= 5.x < 5.10.83
  - **TODO:** May require a specific kernel setting
  - Also see: [NIST NVD entry](https://nvd.nist.gov/vuln/detail/CVE-2022-23222)
- [PwnKit (CVE-2021-4034)](https://github.com/arthepsy/CVE-2021-4034)
  - Affects polkit <= 0.92
  - Also see: [NIST NVD entry](https://nvd.nist.gov/vuln/detail/CVE-2021-4034#match-7844249)
- [NimbusPwn](https://www.microsoft.com/security/blog/2022/04/26/microsoft-finds-new-elevation-of-privilege-linux-vulnerability-nimbuspwn/)
  - Specifically, bug in networkd-dispatcher
- [DirtyCred](https://github.com/Markakd/DirtyCred)
  - UAF vulnerability in Linux credentials handling
  - For more POCs: [2022-LPE-UAF](https://github.com/greek0x0/2022-LPE-UAF)
- [CVE-2021-3156 (Sudo Baron Samedit)](https://github.com/worawit/CVE-2021-3156)
  - Heap-based BOF in sudo versions 1.8.2 < 1.8.32 and 1.9.0 <= 1.9.5
  - Also check out [this exploit writeup](https://datafarm-cybersecurity.medium.com/exploit-writeup-for-cve-2021-3156-sudo-baron-samedit-7a9a4282cb31) (and of course [the original](https://www.qualys.com/2021/01/26/cve-2021-3156/baron-samedit-heap-based-overflow-sudo.txt))
- [How The Tables Have Turned: An analysis of two new Linux vulnerabilities in nf_tables](https://blog.dbouman.nl/2022/04/02/How-The-Tables-Have-Turned-CVE-2022-1015-1016/)

### References

- [Linux LPE](https://book.hacktricks.xyz/linux-unix/privilege-escalation)
- [Qualys' Writeup on Polkit](https://blog.qualys.com/vulnerabilities-threat-research/2022/01/25/pwnkit-local-privilege-escalation-vulnerability-discovered-in-polkits-pkexec-cve-2021-4034)
- [DirtyCred Slides](https://i.blackhat.com/USA-22/Thursday/US-22-Lin-Cautious-A-New-Exploitation-Method.pdf)

### PWK

See this repo for BOF help: https://github.com/gh0x0st/Buffer_Overflow

## Post-Exploit

### General

- [moonwalk](https://github.com/mufeedvh/moonwalk)
  - Watch your back
- [pamspy](https://github.com/citronneur/pamspy)
  - Credentials dumper via eBPF

### Rootkits

- [TripleCross](https://github.com/h3xduck/TripleCross)
  - eBPF rootkit
  - This is the most fully featured I've seen thus far
- [boopkit](https://github.com/kris-nova/boopkit)
  - Backdoor/rootkit (eBPF + TCP)
  - Also see: [bad-bpf](https://github.com/pathtofile/bad-bpf)
- [BPFDoor](https://pastebin.com/kmmJuuQP)
  - APT backdoor triggered by malformed packets to a specific port
  - Also see: [A peek behind the BPFDoor](https://www.elastic.co/blog/a-peek-behind-the-bpfdoor)

### Process Tracing/Injection

- [3snake](https://github.com/blendin/3snake)
  - Extract information from newly spawned processes
- [dlinject](https://github.com/DavidBuchanan314/dlinject)
  - Inject a shared library into a process, but without `ptrace`
  - Python!
- [Cexigua](https://github.com/AonCyberLabs/Cexigua)
  - Code injection w/o `ptrace`
- [linux-inject](https://github.com/gaffe23/linux-inject)
  - Code injection w/o `ptrace`
- [Linux ptrace introduction AKA injecting into sshd for fun](https://blog.xpnsec.com/linux-process-injection-aka-injecting-into-sshd-for-fun/)

### LD_PRELOAD

- ["This will only hurt for a moment": code injection on Linux and macOS with LD_PRELOAD](https://papers.vx-underground.org/papers/Linux/Process%20Injection/%E2%80%9CThis%20will%20only%20hurt%20for%20a%20moment%E2%80%9D_%20code%20injection%20on%20Linux%20and%20macOS%20with%20LD_PRELOAD.pdf)
- [Memory Malware Part 0x2 — Crafting LD_PRELOAD Rootkits in Userland](https://compilepeace.medium.com/memory-malware-part-0x2-writing-userland-rootkits-via-ld-preload-30121c8343d5)

### LDAP

- [ldapsearch](https://man7.org/linux/man-pages/man1/ldapsearch.1.html)
  - Linux utility for LDAP queries
- [ldapper](https://github.com/shellster/LDAPPER)
  - Easier LDAP queries
  - In particular, see [queries.py](https://github.com/shellster/LDAPPER/blob/master/queries.py)
- [ldapdomaindump](https://github.com/dirkjanm/ldapdomaindump)
  - Dump an AD domain

### SSH

- [Dropbear SSH](https://github.com/mkj/dropbear)
  - Mini SSH server + client
- [sshame](https://github.com/HynekPetrak/sshame)
  - SSH pubkey brute force
- [sshkey-grab](https://github.com/NetSPI/sshkey-grab)
  - Requires `gdb`
  - Also see: [Stealing unencrypted SSH-agent keys from memory](https://www.netspi.com/blog/technical/network-penetration-testing/stealing-unencrypted-ssh-agent-keys-from-memory/)
- [ssh-mitm](https://github.com/ssh-mitm/ssh-mitm)
  - Hijack SSH sessions, phish FIDO tokens, snag passwords + (forwarded), snag/manipulate files
- [sshame](https://github.com/HynekPetrak/sshame)
  - Brute-force SSH pubkey auth
- [sshkey-grab](https://github.com/NetSPI/sshkey-grab)
  - Grab keys from agent
  - Also see [NetSPI's article](https://www.netspi.com/blog/technical/network-penetration-testing/stealing-unencrypted-ssh-agent-keys-from-memory/)

### PAM

- [Creating a backdoor in PAM in 5 line of code](https://0x90909090.blogspot.com/2016/06/creating-backdoor-in-pam-in-5-line-of.html)
  - Also see: [linux-pam-backdoor](https://github.com/zephrax/linux-pam-backdoor)
    - **WARNING:** Requires network connectivity so that it can fetch the [PAM source code](https://github.com/linux-pam/linux-pam/blob/master/modules/pam_unix/pam_unix_auth.c#L170-L176), though [the main mechanism](https://github.com/zephrax/linux-pam-backdoor/blob/master/backdoor.sh#L75-L84) is pretty simple to recreate with a custom script.
- [Exfiltrating credentials via PAM backdoors & DNS requests](https://x-c3ll.github.io/posts/PAM-backdoor-DNS/)
  - In particular, see the section on `LD_PRELOAD`
  - This is more geared towards exfiltration of credentials, but it could also be used as a starting point for hooking `pam_sm_authenticate`
  - This could generate some noise, but who's checking for SSH restarts?

### Articles

- [Linux Stealth Rootkit Malware with EDR Evasion | SandFly](https://sandflysecurity.com/blog/linux-stealth-rootkit-malware-with-edr-evasion-analyzed/)
