# Containerization and Orchestration

**TODO:** Some of this belongs in defense

## Containers

- [HackTricks - Docker Breakout / Privilege Escalation](https://book.hacktricks.xyz/linux-hardening/privilege-escalation/docker-breakout/docker-breakout-privilege-escalation)
- [dockle](https://github.com/goodwithtech/dockle)
  - Container image auditor
- [dive](https://github.com/wagoodman/dive)
  - Explore each layer in Docker image
- [dockerscan](https://github.com/cr0hn/dockerscan)
  - Docker security analysis/hacking tools
- [CDK](https://github.com/cdk-team/CDK)
  - Make security testing of K8s, Docker, and Containerd easier
- [amicontainerd](https://github.com/genuinetools/amicontained)
  - Container introspection tool
- [deepce](https://github.com/stealthcopter/deepce)
  - Docker Enumeration, Escalation of Privileges and Container Escapes
- [grype](https://github.com/anchore/grype)
  - Vulnerability scanner for container images and filesystems
- [Distroless Container Images](https://github.com/GoogleContainerTools/distroless)
  - Very, very minimal container OS
- [Docker API RCE.py](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/CVE%20Exploits/Docker%20API%20RCE.py)
  - Python script that uses the Docker API to get a shell on a remote host

## Kubernetes

- [kind (Kubernetes in Docker)](https://github.com/kubernetes-sigs/kind)
  - Spin up a Kubernetes cluster within Docker
  - Makes lab creation **way** simpler
- [kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
- [audit2rbac](https://github.com/liggitt/audit2rbac)
  - Autogenerate RBAC based on audit logs
- [peirates](https://github.com/inguardians/peirates)
  - Kubernetes attack tool
  - Can be manual or automated
- [kyverno](https://github.com/kyverno/kyverno)
  - Policy management
- [Open Policy Agent](https://github.com/open-policy-agent/opa)
  - Policy management via the `rego` language
- [kube2iam](https://github.com/jtblin/kube2iam)
  - AWS IAM cred provider for containers (basically provides better segmentation)
- [kiam](https://github.com/uswitch/kiam)
  - Integrate K8s with AWS IAM
- [k8s-metadata-proxy](https://github.com/GoogleCloudPlatform/k8s-metadata-proxy)
  - Proxy for serving concealed metadata to containers in GCE VM
- [kube-bench](https://github.com/aquasecurity/kube-bench)
  - Audit K8s compliance with CIS benchmarks
- [kube-hunter](https://github.com/aquasecurity/kube-hunter)
  - Hunt for security issues in clusters
- [rbac-police](https://github.com/PaloAltoNetworks/rbac-police)
  - Evaluate RBAC perms of SAs, nodes, and pods via rego policies
- [popeye](https://github.com/derailed/popeye)
  - Cluster resource/config scanner
  - Checks what's running, not what's on disk

## Resources

- [The Use of Name Spaces in Plan 9](https://web.archive.org/web/20140906153815/http://www.cs.bell-labs.com/sys/doc/names.html)
  - Interesting historical tidbit about the development of namespaces + early ideas around containerization
  - More about the system itself: [Plan 9 from Bell Labs](https://en.wikipedia.org/wiki/Plan_9_from_Bell_Labs)
- [DEF CON Cloud Village and Black Hat USA: See New Unit 42 Cloud Research](https://www.paloaltonetworks.com/blog/prisma-cloud/prisma-cloud-def-con-black-hat-usa-cloud-village/)

### Docker

- [Protect the Docker daemon socket](https://docs.docker.com/engine/security/protect-access/)
  - To avoid malicious use of Docker itself, make sure the socket is locked down appropriately
- [Abusing access to mount namespaces through /proc/pid/root](https://labs.withsecure.com/blog/abusing-the-access-to-mount-namespaces-through-procpidroot/)
- [Why is Exposing the Docker Socket a Really Bad Idea?](https://blog.quarkslab.com/why-is-exposing-the-docker-socket-a-really-bad-idea.html)
- [Anatomy of a hack: Docker Registry](https://notsosecure.com/anatomy-of-a-hack-docker-registry)
  - Security concerns related to Docker registries
- [Project Zero - Who Contains the Containers?](https://googleprojectzero.blogspot.com/2021/04/who-contains-containers.html)
  - Windows privesc

### Kubernetes

- [Kubernetes Privilege Escalation: Excessive Permissions in Popular Platforms](https://www.paloaltonetworks.com/resources/whitepapers/kubernetes-privilege-escalation-excessive-permissions-in-popular-platforms)
  - Vulnerability in Azure service fabric
