# Passwords

All things password cracking.

## General

- [hashcat](https://hashcat.net/wiki/doku.php?id=hashcat)
- [hashcat-utils](https://github.com/hashcat/hashcat-utils)
- [princeprocessor](https://hashcat.net/wiki/doku.php?id=princeprocessor)
- [rephraser](https://github.com/travco/rephraser)
- [hydra](https://github.com/vanhauser-thc/thc-hydra)
  - [cheatsheet](https://github.com/frizb/Hydra-Cheatsheet)
- [authoscope](https://github.com/kpcyrd/authoscope)
  - Crack custom web services
- [progpick](https://github.com/kpcyrd/progpick)
  - Bruteforce with a stream of permutations of a specific pattern
- [DeepPass](https://github.com/GhostPack/DeepPass)
  - Identify passwords in documents
- [hashcat-cheatsheet](https://github.com/frizb/Hashcat-Cheatsheet)
- [pdfrip](https://github.com/mufeedvh/pdfrip)
  - Crack PDFs
- [hcre](https://github.com/KMikeeU/hcre)
  - Standalone Hashcat rules engine in Python as a CLI utility
- [Name-That-Hash](https://github.com/HashPals/Name-That-Hash)
  - Hash type identifier
- [Search-That-Hash](https://github.com/HashPals/Search-That-Hash)
  - Automatically search APIs for a given hash and crack it if not found
  - Mainly useful as a reference, especially for [API calls](https://github.com/HashPals/Search-That-Hash/blob/main/search_that_hash/cracker/cracking.py)
- [CredMaster](https://github.com/knavesec/CredMaster)
  - Password spray/brute force tool that shifts IP via AWS passthrough proxies
- [KeePass Password Dumper (CVE-2023-32784)](https://github.com/vdohney/keepass-password-dumper)
  - Fixed in KeePass 2.54
- [KeeFarce](https://github.com/denandz/KeeFarcehttps://github.com/denandz/KeeFarce)
  - Extract passwords from a KeePass 2.x database directly from memory
- [npk](https://github.com/Coalfire-Research/npk)
  - Serverless + distributed hash cracking platform (!)
- [changeme](https://github.com/ztgrace/changeme)
  - Default credential scanner
- [vast.ai](https://vast.ai)
  - Cheap GPU rentals

## Rules

- [OneRuleToRuleThemAll](https://github.com/NotSoSecure/password_cracking_rules)

## Wordlists

- [SecLists](https://github.com/danielmiessler/SecLists/tree/master/Passwords)
- [Default Creds Cheatsheet](https://github.com/ihebski/DefaultCreds-cheat-sheet)
- [CrackStation](https://crackstation.net/crackstation-wordlist-password-cracking-dictionary.htm)
  - Aggregation of various wordlists
- [WeakPass](https://weakpass.com/)
  - Many different wordlists + aggregations
- [Probable Wordlists](https://github.com/berzerk0/Probable-Wordlists)
  - Statistically common passwords
  - Beware that this requires torrent and/or mega.nz download (or just use WeakPass)
- [passphrase-wordlist](https://github.com/initstring/passphrase-wordlist)
  - Wordlist for more uncommon words, e.g. some words are sourced from KnowYourMeme
- [CeWL](https://github.com/digininja/CeWL)
  - Custom Word List generator
- [D4N155](https://github.com/OWASP/D4N155)
  - Wordlist based on website contents
- [Wikimedia Dumps](https://dumps.wikimedia.org/)
  - You could probably pull something out of this
- [DBpedia](https://www.dbpedia.org/about/)
  - Structured data from Wikimedia projects, exposes a SPARQL endpoint for querying
- [english-words](https://github.com/dwyl/english-words)
  - A text file with 466k English words
- [Google Books n-gram frequency lists](https://github.com/orgtre/google-books-ngram-frequency)
- [Rocktastic (Nettitude)](https://labs.nettitude.com/tools/rocktastic/)

### Books

- Project Gutenberg
  - [SO: How to download all English books from Gutenberg?](https://webapps.stackexchange.com/a/12312)
  - Two options:
    - For specific files/filetypes, or smaller sets in general: [Information About Robot Access to our Pages](https://www.gutenberg.org/policy/robot_access.html)
    - For the entire collection: [Mirroring How-To](https://www.gutenberg.org/help/mirroring.html)

### Movie/TV Scripts

**TODO:** Need to find a way to extract text from PDFs...

- [IMSDb](https://imsdb.com)
  - All the movie/TV scripts
  - Use [imsdb_download_all_scripts](https://github.com/j2kun/imsdb_download_all_scripts) to scrape the website. If it doesn't work for whatever reason, it's super easy to scrape the website anyway.
- [AwesomeFilm.com](https://www.awesomefilm.com/)
  - Scripts as plain text files
- [Script on Screen](https://scripts-onscreen.com/)
  - Search engine for movie scripts if a specific one is needed
- [The Daily Script](https://www.dailyscript.com/links.html)
  - List of resources for finding scripts


### Song Lyrics

- [musiXmatch](http://millionsongdataset.com/musixmatch/)
- [Music Dataset: Lyrics and Metadata from 1950 to 2019](https://data.mendeley.com/datasets/3t9vbwxgr5/2)
- Kaggle is pretty handy!
  - https://www.kaggle.com/datasets/deepshah16/song-lyrics-dataset
  - https://www.kaggle.com/datasets/neisse/scrapped-lyrics-from-6-genres
  - https://www.kaggle.com/datasets/paultimothymooney/poetry
  - https://www.kaggle.com/datasets/terminate9298/songs-lyrics
