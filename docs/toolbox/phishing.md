# Phishing

- [GoPhish](https://github.com/gophish/gophish)
  - Open-source phish framework
- [SingleFile](https://chrome.google.com/webstore/detail/singlefile/mpiodijhokgodhhofbcjdecpffjipkle?hl=en)
  - Clone a webpage, save as HTML
- [wifiphisher](https://github.com/wifiphisher/wifiphisher)
  - Rogue access point
- [LinkedInt](https://github.com/vysecurity/LinkedInt)
  - LinkedIn recon
- [hunter.io](https://hunter.io/)
  - Find professional email addresses
- [nexphisher](https://github.com/htr-tech/nexphisher)
  - Phishing via CLI
- [Expired Domains](https://www.expireddomains.net/)
  - Handy for getting past reputation-based web filters
  - Also see: [urlscan.io](https://urlscan.io/)
- [Follina](https://doublepulsar.com/follina-a-microsoft-office-code-execution-vulnerability-1a47fce5629e)
  - MSProtocol URI abuse
- [malicious-pdf](https://github.com/jonaslejon/malicious-pdf)
  - Generate malicious PDFs
- [Right-to-Left Override (RTLO)](https://blog.malwarebytes.com/cybercrime/2014/01/the-rtlo-method/)
  - Obfuscation technique to hide the true extension of a file
  - For example, take the filename `filefdp.exe`. Let's insert the RTLO unicode so we get `file[]fdp.exe` - now it'll render as `fileexe.pdf`
- [BITB Attack Templates](https://github.com/mrd0x/BITB)
  - Pretend you're one of those SSO popup windows
- [filesec.io](https://filesec.io/)
  - File extensions that could be used in attacks
- [evilnginx2](https://github.com/kgretzky/evilginx2)
  - MITM proxy/framework
- [murarena](https://github.com/muraenateam/muraena)
  - Reverse proxy meant for automating phishing + post activities
- [Mailgun](https://www.mailgun.com/)
  - Email service with an API

## Resources

- [Long Live DMARC - Email Spoof issues](https://www.redteam.cafe/phishing/long-live-dmarc-email-spoof-issues)
  - Lots of other interesting-looking pages as well
- [SentinelOne: Malicious PDF Techniques](https://www.sentinelone.com/blog/malicious-pdfs-revealing-techniques-behind-attacks/)
- [SentinelOne: Paypal Phishing Scam Analysis](https://www.sentinelone.com/blog/technical-analysis-paypal-phishing-scam/)
- [Why BitB Attacks are Concerning](https://www.phishlabs.com/blog/why-bitb-attacks-are-concerning/)
- [MS Teams Attachment Spoofing](https://medium.com/@bobbyrsec/microsoft-teams-attachment-spoofing-and-lack-of-permissions-enforcement-leads-to-rce-via-ntlm-458aea1826c5)
- [Abusing the MS Office protocol scheme](https://blog.syss.com/posts/abusing-ms-office-protos/)
- [Phishing With Chromium's Application Mode](https://mrd0x.com/phishing-with-chromium-application-mode/)
- [PNG Steganography](https://decoded.avast.io/martinchlumecky/png-steganography/)
