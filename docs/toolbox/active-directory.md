# Active Directory

- [BloodHound](https://github.com/BloodHoundAD/BloodHound)
  - Graph out AD relationships
  - For testing and a Python script w/ queries (!), see: [BloodHound-Tools](https://github.com/BloodHoundAD/BloodHound-Tools)
- [Active Directory Integrated DNS dump tool](https://github.com/dirkjanm/adidnsdump)
  - Also see [this article](https://dirkjanm.io/getting-in-the-zone-dumping-active-directory-dns-with-adidnsdump/)
- [Kerberoasting](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/t1208-kerberoasting)
  - Explainer that looks decent: [Kerberoasting Attacks Explained](https://www.qomplx.com/qomplx-knowledge-kerberoasting-attacks-explained/)
- [ADRecon](https://github.com/adrecon/ADRecon)
  - Does what you think
- [ADSearch](https://github.com/tomcarver16/ADSearch)
  - Tool written for Cobalt Strike \<command\> to make things more efficient
- [windapsearch](https://github.com/ropnop/windapsearch)
  - Enumerate Windows domain
- [Grouper](https://github.com/l0ss/Grouper2)
  - AD group policy checks
- [generate-ad-username](https://github.com/w0Tx/generate-ad-username)
  - Also see: [Username Anarchy](https://github.com/urbanadventurer/username-anarchy)
- [Script for spraying empty password](https://github.com/S3cur3Th1sSh1t/Creds/blob/master/PowershellScripts/Invoke-SprayEmptyPassword.ps1)
- [Active Directory Integrated DNS dump tool](https://github.com/dirkjanm/adidnsdump)
  - Export all DNS records in the domain or forest DNS zones
- [adPEAS](https://github.com/61106960/adPEAS)
  - Wraps multiple common tools
- [PingCastle](https://github.com/vletoux/pingcastle)
  - AD audit tool
- [AD Explorer (Sysinternals)](https://docs.microsoft.com/en-us/sysinternals/downloads/adexplorer)
  - GUI-based tools for exploring a domain
- [AD-control-paths](https://github.com/ANSSI-FR/AD-control-paths)
  - Similar to BloodHound
- [bloodyAD](https://github.com/CravateRouge/bloodyAD)
  - AD privilege escalation framework
- [ASREPRoast](https://book.hacktricks.xyz/windows-hardening/active-directory-methodology/asreproast)
  - For users without attribute requiring Kerberos pre-auth
- [Shadow Credentials](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/shadow-credentials)
  - Requirement: editable `msDS-KeyCredentialLink`
- [kerbrute](https://github.com/ropnop/kerbrute)
  - Kerberos pre-auth bruteforcing
- [Powermad](https://github.com/Kevin-Robertson/Powermad)
  - MachineAccountQuota and DNS exploit tools
- [Adalanche](https://github.com/lkarlslund/Adalanche)
  - AD ACL visualization/explorer
- [Group3r](https://github.com/Group3r/Group3r)
  - Find vulns in AD Group Policy
- [Roast in the Middle](https://github.com/0xe7/RoastInTheMiddle)
  - Original article: [New Attack Paths? AS Requested Service Tickets](https://www.semperis.com/blog/new-attack-paths-as-requested-sts/)
  - [Python implementation](https://github.com/Tw1sm/RITM)
- [AD_delegation_hunting](https://github.com/NotSoSecure/AD_delegation_hunting)
  - Tool by NotSoSecure that automates hunting for AD delegation access
- [ADACLScanner](https://github.com/canix1/ADACLScanner)
  - Scan AD ACLs + profit
- [certsync](https://github.com/zblurx/certsync)
  - Dump NTDS with golden certificates and UnPAC the hash
- [ADExplorter on Engagements](https://www.trustedsec.com/blog/adexplorer-on-engagements/)
- [certify](https://github.com/GhostPack/Certify)
  - ADCS enum + abuse
- [certipy](https://github.com/ly4k/Certipy)
  - ADCS enum + abuse
- [DCShadow: detecting a rogue domain controller replicating malicious changes to your Active Directory](https://medium.com/@maarten.goet/dcshadow-detecting-a-rogue-domain-controller-replicating-malicious-changes-to-your-active-1e22440df9ad)
  - [dcshadow.com](https://www.dcshadow.com/)

## Resources/Articles

- [AD Resources](https://adsecurity.org/?page_id=41)
- [Exploring Users With Multiple Accounts In BloodHound](https://insomniasec.com/blog/bloodhound-shared-accounts)
- [MachineAccountQuota is USEFUL Sometimes: Exploiting One of Active Directory's Oddest Settings](https://www.netspi.com/blog/technical/network-penetration-testing/machineaccountquota-is-useful-sometimes/)
  - Written by the same person that wrote PowerMad
- [AD cheatsheet](https://wadcoms.github.io/)
- [AD methodology](https://book.hacktricks.xyz/windows/active-directory-methodology)
- [Active Directory Security](https://adsecurity.org/)
- [Getting in the Zone: dumping Active Directory DNS using adidnsdump](https://dirkjanm.io/getting-in-the-zone-dumping-active-directory-dns-with-adidnsdump/)
- [AS-REP Roasting](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/as-rep-roasting-using-rubeus-and-hashcat)
  - Note the extra reference at the bottom: [Kerberos AD Attacks - More Roasting with AS-REP](https://blog.xpnsec.com/kerberos-attacks-part-2/)

### ADCS

- [Certified Pre-Owned Abusing Active Directory Certificate Services](https://specterops.io/assets/resources/Certified_Pre-Owned.pdf)
  - Shorter article on Medium: https://posts.specterops.io/certified-pre-owned-d95910965cd2
- [Active Directory Certificate Services: Risky Settings and How to Remediate Them](https://blog.netwrix.com/2021/08/24/active-directory-certificate-services-risky-settings-and-how-to-remediate-them/)
- [ADCS + PetitPotam NTLM Relay: Obtaining krbtgt Hash with Domain Controller Machine Certificate](https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/adcs-+-petitpotam-ntlm-relay-obtaining-krbtgt-hash-with-domain-controller-machine-certificate)
