---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline:
actions:
  - text: InfoSec Toolbox →
    link: /toolbox/README.md
    type: secondary
  - text: Tutorials →
    link: /tutorials/README.md
    type: secondary
  - text: Security Experiments →
    link: https://gitlab.com/zach.sanchez113/security-experiments/
    type: secondary
footer: Made by [human] with ❤️
---


Welcome!

Here you will find content related to information security, systems administration, software development, and
similar topics.

Hope you enjoy and that the information inside proves useful!

::: warning Disclaimer

As always, only use this website to add value to the world. Never perform any malicious activities described
here outside of a lab environment or without the explicit permission of whoever owns the system you want to
hack.

:::
