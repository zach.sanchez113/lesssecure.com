import { defineUserConfig } from 'vuepress';
import { defaultTheme } from 'vuepress';
import { searchPlugin } from '@vuepress/plugin-search';

export default defineUserConfig({
  lang: 'en-US',
  title: 'Less Secure',
  description: 'Just playing around',

  // Extract all the headers
  markdown: {
    headers: { level: [2, 3, 4, 5] },
  },

  plugins: [
    searchPlugin({
      // Placeholder text is always nice
      locales: {
        '/': {
          placeholder: 'Search',
        },
      },

      // Search page contents as well
      getExtraFields: (page) => [page.content],

      // Default is too few
      maxSuggestions: 10,
    }),
  ],

  theme: defaultTheme({
    contributors: false,
    editLink: false,
    navbar: [
      { text: 'Home', link: '/' },
      { text: 'Toolbox', link: '/toolbox/' },
      { text: 'Tutorials', link: '/tutorials/' },
      { text: 'Security Experiments', link: 'https://gitlab.com/zach.sanchez113/security-experiments/' },
      { text: 'GitLab', link: 'https://gitlab.com/zach.sanchez113' },
    ],
    sidebarDepth: 5,
    sidebar: [
      {
        text: 'Notes',
        collapsible: true,
        children: [
          {
            text: 'Development',
            collapsible: true,
            children: [
              '/notes/development/README.md',
              '/notes/development/bash.md',
              '/notes/development/data-science.md',
              '/notes/development/databases.md',
              '/notes/development/frontend.md',
              '/notes/development/python.md',
              '/notes/development/rust.md',
            ],
          },
          '/notes/devops.md',
          '/notes/homelab.md',
          '/notes/linux.md',
          '/notes/system-utils.md',
          {
            text: 'Windows',
            collapsible: true,
            children: [
              '/notes/windows/README.md',
              '/notes/windows/ese.md',
              '/notes/windows/linux-to-windows.md',
              '/notes/windows/networking.md',
            ],
          },
        ],
      },
      {
        text: 'Toolbox',
        collapsible: true,
        children: [
          '/toolbox/README.md',
          '/toolbox/active-directory.md',
          '/toolbox/containers.md',
          '/toolbox/defense.md',
          '/toolbox/injection.md',
          '/toolbox/iot.md',
          '/toolbox/linux.md',
          '/toolbox/misc.md',
          '/toolbox/mobile.md',
          '/toolbox/passwords.md',
          '/toolbox/phishing.md',
          {
            text: 'Privilege Escalation',
            collapsible: true,
            children: ['/toolbox/privesc/osx.md', '/toolbox/privesc/other.md'],
          },
          '/toolbox/post-exploit/README.md',
          {
            text: 'Resources',
            collapsible: true,
            children: [
              '/toolbox/resources/README.md',
              '/toolbox/resources/appsec.md',
              '/toolbox/resources/cloudsec.md',
              '/toolbox/resources/malware.md',
              '/toolbox/resources/redteam.md',
            ],
          },
          {
            text: 'Scanning',
            collapsible: true,
            children: [
              '/toolbox/scanning/README.md',
              '/toolbox/scanning/cloud.md',
              '/toolbox/scanning/network.md',
              '/toolbox/scanning/osint.md',
              '/toolbox/scanning/web.md',
              '/toolbox/scanning/windows.md',
            ],
          },
          '/toolbox/sdlc.md',
          '/toolbox/ssh.md',
          '/toolbox/todo.md',
          '/toolbox/windows.md',
        ],
      },
      {
        text: 'Tutorials',
        collapsible: true,
        children: [
          '/tutorials/README.md',
          '/tutorials/containers.md',
          '/tutorials/kubernetes.md',
          '/tutorials/linux-evasion.md',
          '/tutorials/password-cracking.md',
          '/tutorials/shells.md',
        ],
      },
    ],
    // repo: 'https://gitlab.com/zach.sanchez113/lesssecure.com',
  }),
});
